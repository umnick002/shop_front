import React from "react";
import CategoryProducts from "./CategoryProducts";
import SubCategoryList from "./SubCategoryList";
import CategoryList from "./CategoryList";

class Category extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            category: {
                id: 0,
                subCategories: []
            }
        };
        this.onCategoryChange = this.onCategoryChange.bind(this);
    }

    componentDidMount() {
        window.onpopstate = (e) => {
            if(e.state && e.state.categoryId) {
                window.location.pathname = `/categories/${parseInt(e.state.categoryId)}`;
            }
        };
    }

    componentWillUnmount() {
        window.onpopstate = () => {}
    }

    onCategoryChange(category) {
        this.setState({
            category: category
        })
    }

    static back() {
        let categoryId = window.location.pathname.slice(window.location.pathname.lastIndexOf('/') + 1);
        if (Number.isInteger(categoryId) && parseInt(categoryId) % 10 !== 0) {
            categoryId = parseInt(parseInt(categoryId) / 100) * 100;
            window.location = '/categories/' + categoryId;
        } else {
            window.location = '/';
        }
    }

    render() {
        if (isMobile === true) {
            const categories = this.props.categories || [];
            for (let category of categories) {
                if (this.state.category.id === category.id) {
                    this.state.category.subCategories = category.subCategories;
                    break
                }
            }
        }
        return (
            isMobile === false ?
                this.props.main ?
                    <div className="grid-x small-12">
                        <CategoryList categories={this.props.categories}/>
                    </div>
                    :
                    <div className="grid-x small-12">
                        <div className="grid-y small-3">
                            <div>
                                <button className="back-button" onClick={() => Category.back()}>
                                    <i className="fas fa-chevron-left"/>
                                    Назад
                                </button>
                            </div>
                            <SubCategoryList category={this.state.category} onCategoryChange={this.onCategoryChange}/>
                        </div>
                        <CategoryProducts customer={this.props.customer} category={this.state.category}
                                          onChangeFavorite={this.props.onChangeFavorite}
                                          onChangeOrder={this.props.onChangeOrder}/>
                    </div>
            :
                this.props.main ?
                    <div>

                    </div>
                    :
                    <div className="grid-x">
                        <CategoryProducts customer={this.props.customer} category={this.props.category} categories={this.props.categories}
                                          onChangeFavorite={this.props.onChangeFavorite} onChangeCategory={this.props.onChangeCategory}
                                          onChangeOrder={this.props.onChangeOrder}/>
                    </div>
        )
    }
}

export default Category;