import React from "react";
import CategoryList from "./CategoryList";

class CategoryPage extends React.Component {
    render() {
        const categories = this.props.categories || [];
        return (
            isMobile === false ?
            <div className="grid-x">
                <div className="grid-x small-3">
                    <CategoryList categories={categories}/>
                </div>
                <div className="grid-x small-9 align-justify align-self-top">
                    <div className='small-12 page-title'>
                        Каталог
                    </div>
                    {categories.map(category =>
                        <a key={category.id} className='grid-x small-4 category-card'
                           href={`/categories/${category.id*100}`}>
                            <div className='grid-x align-middle'>
                                <div className="small-4 text-center">
                                    <i className="fas fa-4x fa-apple-alt"/>
                                </div>
                                <div className="small-8 align-middle category-card-name">
                                    <span>{category.name}</span>
                                </div>
                            </div>
                        </a>
                    )}
                </div>
            </div>
                :
            <div className="grid-x small-12 align-spaced">
                <div className='small-12 page-title'>
                    Каталог
                </div>
                {categories.map(category =>
                    <a key={category.id} className='grid-x small-5 category-card box-shadow'
                       href={`/categories/${category.id*100}`}>
                        <div className='grid-y align-middle'>
                            <div className='grid-x'>
                                <div className="grid-x small-12 align-center bottom-border category-icon">
                                    <i className="fas fa-3x fa-apple-alt"/>
                                </div>
                                <div className="grid-x small-12 text-center">
                                    <span>{category.name}</span>
                                </div>
                            </div>
                        </div>
                    </a>
                )}
            </div>
        )
    }
}

export default CategoryPage;