import React from "react";

class SubCategoryList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            category: {
                id: 0,
                subCategories: []
            }
        }
    }

    componentDidMount() {
        fetch(host + '/category-list/' +
            parseInt(window.location.pathname.slice(window.location.pathname.lastIndexOf('/') + 1))
        ).then(response => {
            return response.json();
        }).then(data => {
            console.log(data);
            this.props.onCategoryChange(data);
        })
    }

    render() {
        const subCategories = this.props.category.subCategories;
        const subCategoryId = parseInt(window.location.pathname.slice(window.location.pathname.lastIndexOf('/') + 1))%100;
        return (
            <div className="grid-x small-3 category-list" style={{height: "fit-content", paddingRight: "0.6rem"}}>
                <div className="grid-y small-12" style={{fontSize: ".9rem", lineHeight: "1.2rem", color: "#3c3c3c"}}>
                    <div className="subcategory-name" style={{backgroundColor: "#eee", padding: "0.6rem 0.6rem", position: "relative"}}>
                        <a href={parseInt(this.props.category.id) * 100}>
                            {this.props.category.name}
                        </a>
                    </div>
                    {subCategories.map((subCategory) =>
                        <div className="small-offset-1 subcategory-item" key={subCategory.id}>
                            <a className="grid-y" href={parseInt(this.props.category.id * 100 + subCategory.id)}
                               style={subCategory.id === subCategoryId ? {fontWeight: "bold"} : {}}>{subCategory.name}</a>
                        </div>
                    )}
                </div>
            </div>
        )
    }
}

export default SubCategoryList;