import React from "react";

class CategoryLine extends React.Component {
    constructor(props) {
        super(props);
        let id = parseInt(window.location.pathname.slice(window.location.pathname.lastIndexOf('/') + 1));
        this.state = {
            showDropdown: id%100 === 0
        }
    }

    onCategoryClick() {
        this.setState({
            showDropdown: !this.state.showDropdown
        })
    }

    onChangeCategory(category, subCategory, withSubCategory) {
        let cat = {...category};
        cat.subCategories = [subCategory];
        this.props.onChangeCategory(cat, withSubCategory);
        this.onCategoryClick();
    }

    render() {
        let id = parseInt(window.location.pathname.slice(window.location.pathname.lastIndexOf('/') + 1));
        let category = this.props.category || {id: 0, subCategories: []};
        let subCategories = [];
        if (isMobile === true && this.props.category !== undefined && this.props.categories !== undefined) {
            for (let cat of this.props.categories) {
                if (cat.id === this.props.category.id) {
                    subCategories = cat.subCategories;
                    break;
                }
            }
        }
        let categoryLine = [
            <div key={-1}>
                <a href={'/categories'}>Категории</a>
            </div>,
        ];
        let name = 'Категории';
        if (isMobile === false) {
            if (id%100 !== 0) {
                categoryLine.push(
                    <div className="category-line-item" key={category.id * 100}>
                        <i className="fas fa-chevron-right"/>
                            <a href={category.id * 100}>{category.name}</a>
                    </div>
                );
                for (let i in category.subCategories) {
                    if (id === (category.id * 100 + category.subCategories[i].id)) {
                        name = category.subCategories[i].name;
                        categoryLine.push(
                            <div key={category.id * 100 + category.subCategories[i].id}>
                                <i className="fas fa-chevron-right"/>
                                <span>{name}</span>
                            </div>
                        );
                        break;
                    }
                }
            } else {
                name = category.name;
                categoryLine.push(
                    <div className="category-line-item" key={category.id * 100}>
                        <i className="fas fa-chevron-right"/>
                        <span>{name}</span>
                    </div>
                );
            }
        } else if (category.subCategories !== undefined && category.subCategories.length > 0) {
            name = category.subCategories[0].name;
            categoryLine.push(
                <div className="category-line-item" key={category.id * 100}>
                    <i className="fas fa-chevron-right"/>
                    <span onClick={() => this.props.onChangeCategory(category, false)}>{category.name}</span>
                </div>,
                <div key={category.id * 100 + category.subCategories[0].id}>
                    <i className="fas fa-chevron-right"/>
                    <span>{name}</span>
                </div>
            );
        } else {
            name = category.name;
            categoryLine.push(
                <div key={category.id * 100}>
                    <i className="fas fa-chevron-right"/>
                    <span>{name}</span>
                </div>
            );
        }
        return (
            <div className={"grid-x small-12 category-line"}>
                <div className={"grid-x scrollable"}>
                    <div className={"grid-x"}>
                        {categoryLine}
                    </div>
                </div>
                <div className="small-12 category-name" onClick={subCategories.length > 0 ? () => this.onCategoryClick() : null}>
                    {name}
                    {subCategories.length > 0 ? <i className={"fas " + (this.state.showDropdown === true ? 'fa-chevron-up' : 'fa-chevron-down')}/>:null}
                </div>
                {subCategories.length > 0 ?
                    <div className={'small-12 subcategory-items ' + (this.state.showDropdown === true ? 'grid-y' : 'hide')}>
                        {subCategories.map((subCategory) =>
                            <div key={subCategory.id} className='grid-x subcategory-item'>
                                <div className={'small-12 ' + (subCategory.name === name ? 'selected' : '')}
                                     onClick={() => this.onChangeCategory(category, subCategory, true)}>
                                    {subCategory.name}
                                </div>
                            </div>
                        )}
                    </div>
                :null}
            </div>
        )
    }
}

export default CategoryLine;