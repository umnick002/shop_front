import React from "react";
import ProductVolumes from "../product/ProductVolumes";
import ProductPrice from "../product/ProductPrice";
import ProductToCart from "../product/ProductToCart";
import Stars from "../product/Stars";

class ProductCard extends React.Component {
    constructor(props) {
        super(props);
        this.handleClickProduct = this.handleClickProduct.bind(this);
        this.state = {
            active: -1,
        }
    }

    handleClickProduct(i) {
        this.setState({
            active: i
        })
    }

    handleEnableFavorite(target) {
        if (this.props.favorite === false) {
            target.classList.remove('far');
            target.classList.add('fas');
        }
    }

    handleDisableFavorite(target) {
        if (this.props.favorite === false) {
            target.classList.remove('fas');
            target.classList.add('far');
        }
    }

    handleFavorite() {
        fetch(host + '/favorites/' + this.props.commonProduct.id, {
            method: this.props.favorite ? 'DELETE' : 'POST'
        })
            .then((response) => {
                if (response.status === 200) {
                    this.props.handleFavorite(this.props.commonProduct.id)
                } else if (response.status === 401) {
                    document.querySelector('#auth').classList.remove('hide');
                    document.querySelector('#auth').classList.add('grid-x');
                }
            });
    }

    render() {
        const commonProduct = this.props.commonProduct;
        let active;
        if (this.state.active !== -1) {
            active = this.state.active;
        } else if (commonProduct.products.length > 0) {
            active = 0;
        } else {
            active = -1;
        }
        let orderItem = {};
        this.props.orderItems.forEach(oi => {
            if (oi.sellPrice.product.id === commonProduct.products[active].id) {
                orderItem = oi;
            }
        });
        return (
            isMobile === false ?
            <div className="cell grid-x product-card">
                <button onMouseOver={(e) => this.handleEnableFavorite(e.target)}
                        onMouseOut={(e) => this.handleDisableFavorite(e.target)}
                        onClick={(e) => this.handleFavorite(e.target)}>
                    <i className={(this.props.favorite ? "fas" : "far invisible ") + " fa-lg fa-heart"}/>
                </button>
                <div className="small-12" style={{boxShadow: "inset 0 0 0 .1rem #dcdcdc", padding: "0.4rem 0.6rem 1rem 0.6rem"}}>
                    <a href={"/products/" + commonProduct.id} target="_blank" rel="noopener noreferrer"
                       className="grid-x align-center"
                       style={{marginBottom: "0.6rem"}}>
                        {commonProduct.contents !== undefined && commonProduct.contents.length > 0 ?
                        <img src={staticHost + commonProduct.contents[0].file}
                             style={{minHeight: "10rem", maxHeight: "10rem", cursor: "pointer"}}/>
                        : <i className="far fa-2x fa-image"/>
                        }
                    </a>
                    <div className="grid-x align-center">
                        <ProductVolumes handleClickProduct={this.handleClickProduct} products={commonProduct.products}
                                        active={active}/>
                    </div>
                    <div className="grid-x align-center" style={{fontSize: "0.8rem", height: "3.3rem", lineHeight: "1.15rem"}}>
                        <a href={"/products/" + commonProduct.id} target="_blank" rel="noopener noreferrer"
                           className="small-12 text-center">{commonProduct.name}</a>
                    </div>
                    <div className="grid-x align-center rating-block">
                        <Stars rating={Math.ceil(this.props.commonProduct.rating || 0)} edit={false}/>
                    </div>
                    <div>
                        {commonProduct.products[active].reserved ?
                            <div className="grid-x">
                                <ProductPrice product={commonProduct.products[active]}/>
                                <div className="grid-y small-6 align-right">
                                    {
                                        commonProduct.products[active].number !== undefined ?
                                            <div className="cell text-right"
                                                 style={{fontSize: "0.8rem", color: "#777"}}>
                                                Артикул: {commonProduct.products[active].number}
                                            </div>
                                            : null
                                    }
                                    <div className="product-to-cart cell grid-x align-right">
                                        <ProductToCart orderItem={orderItem}
                                                       activeProduct={commonProduct.products[active]}
                                                       onChangeOrder={this.props.onChangeOrder}/>
                                    </div>
                                </div>
                            </div>
                            :
                            <div style={{color: '#cc4b37', marginTop: '0.4rem', fontWeight: '600'}}>
                                Нет в наличии
                            </div>
                        }
                    </div>
                </div>
            </div>
                :
                <div className="cell grid-x product-card mobile align-center">
                    <div className="small-11 item">
                        <div className="grid-x">
                            <div className="grid-x small-3 align-center">
                                <a href={"/products/" + commonProduct.id}
                                   style={{marginBottom: "0.6rem"}}>
                                    {commonProduct.contents !== undefined && commonProduct.contents.length > 0 ?
                                        <img src={staticHost + commonProduct.contents[0].file}
                                             style={{minHeight: "5rem", maxHeight: "5rem", cursor: "pointer"}}/>
                                        : <i className="far fa-2x fa-image"/>
                                    }
                                </a>
                            </div>
                            <div className="grid-x small-9">
                                <div className="grid-x small-12">
                                    <div className="grid-x small-12 align-center" style={{fontSize: "0.8rem", height: "3.3rem", lineHeight: "1.15rem"}}>
                                        <a href={"/products/" + commonProduct.id}
                                           className="small-12 text-center">{commonProduct.name}</a>
                                    </div>
                                    <div className="grid-x small-12 align-center">
                                        <ProductVolumes handleClickProduct={this.handleClickProduct} products={commonProduct.products}
                                                        active={active}/>
                                    </div>
                                    <div className="grid-x small-12 align-center rating-block">
                                        <Stars rating={Math.ceil(this.props.commonProduct.rating || 0)} edit={false}/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div>
                            {commonProduct.products[active].reserved ?
                                <div className="grid-x">
                                    <ProductPrice product={commonProduct.products[active]}/>
                                    <div className="grid-y small-6 align-right">
                                        <div className="product-to-cart cell grid-x align-right">
                                            <ProductToCart orderItem={orderItem}
                                                           activeProduct={commonProduct.products[active]}
                                                           onChangeOrder={this.props.onChangeOrder}/>
                                        </div>
                                    </div>
                                </div>
                                :
                                <div style={{color: '#cc4b37', marginTop: '0.4rem', fontWeight: '600'}}>
                                    Нет в наличии
                                </div>
                            }
                        </div>
                    </div>
                </div>
        )
    }
}

export default ProductCard;