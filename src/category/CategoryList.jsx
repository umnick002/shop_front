import React from "react";

class CategoryList extends React.Component {
    render() {
        const categories = this.props.categories || [];
        return (
            <div className="grid-x category-list" style={{height: "fit-content", paddingRight: "1rem"}}>
                <div className="grid-y" style={{fontSize: "1rem", lineHeight: "2rem", color: "#3c3c3c"}}>
                    {categories.map((category) =>
                        <div className="small-offset-2" key={category.id}>
                            <a href={'categories/' + category.id * 100}>{category.name}</a>
                        </div>
                    )}
                </div>
            </div>
        )
    }
}

export default CategoryList;