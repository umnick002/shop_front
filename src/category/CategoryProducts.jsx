import React from "react";
import ProductCard from "./ProductCard";
import CategoryLine from "./CategoryLine";

class CategoryProducts extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            commonProducts: [],
        };
        this.handleFavorite = this.handleFavorite.bind(this);
        this.onChangeOrder = this.onChangeOrder.bind(this);
    }

    componentDidMount() {
        if (isMobile === false) {
            fetch(host + window.location.pathname).then(response => {
                return response.json();
            }).then(data => {
                console.log(data);
                this.setState({
                    commonProducts: data
                });
                document.querySelector("body").hidden = false;
            });
        }
    }

    componentDidUpdate(prevProps) {
        if (isMobile === true) {
            const isFirstCategory = prevProps.category === undefined && this.props.category !== undefined;
            const isFirstSubCategory = prevProps.category !== undefined && (prevProps.category.subCategories === undefined && this.props.category.subCategories !== undefined || prevProps.category.subCategories !== undefined && this.props.category.subCategories === undefined);
            const isDifferentCategory = prevProps.category !== undefined && this.props.category !== undefined && prevProps.category.id !== this.props.category.id;
            const isDifferentSubCategory = prevProps.category !== undefined && this.props.category !== undefined && prevProps.category.subCategories !== undefined && this.props.category.subCategories !== undefined && this.props.category.subCategories[0].id !== prevProps.category.subCategories[0].id;
            if (isFirstCategory === true || isFirstSubCategory === true || isDifferentCategory === true || isDifferentSubCategory === true) {
                this.getData(this.props.category, this.props.category.subCategories !== undefined ? this.props.category.subCategories[0] : {id: 0})
            }
        }
    }

    getData(category, subCategory) {
        fetch(host + `/categories/${parseInt(category.id*100) + subCategory.id}`).then(response => {
            return response.json();
        }).then(data => {
            console.log(data);
            this.setState({
                commonProducts: data
            });
            document.querySelector("body").hidden = false;
        });
    }

    handleFavorite(id) {
        let favoriteIds = new Set();
        const customer = this.props.customer;
        if (customer.favorites !== undefined) {
            customer.favorites.forEach(item => {
                favoriteIds.add(item.favoriteId)
            });
        }
        if (favoriteIds.has(id)) {
            favoriteIds.delete(id);
        } else {
            favoriteIds.add(id)
        }
        let favorites = [];
        favoriteIds.forEach(item => {favorites.push({favoriteId: item})});
        this.props.onChangeFavorite(favorites);
    }

    onChangeOrder(product, volume) {
        this.props.onChangeOrder(product, volume);
    }

    getMatchedOrderItems(commonProduct) {
        let matched = [];
        if (this.props.customer.orders !== undefined && this.props.customer.orders[0].customerOrderItems !== undefined) {
            this.props.customer.orders[0].customerOrderItems.forEach(orderItem => {
                commonProduct.products.forEach(product => {
                    if (orderItem.sellPrice.product.id === product.id) {
                        matched.push(orderItem);
                    }
                })
            });
        }
        return matched;
    }

    render() {
        const commonProducts = this.state.commonProducts;
        let favorites = [];
        const customer = this.props.customer;
        if (customer.favorites !== undefined) {
            customer.favorites.forEach(item => {
                favorites.push(item.favoriteId)
            });
        }
        let orders = [];
        (this.props.customer.orders || []).forEach(order => {
            orders.push(order.id)
        });
        let orderItems = [];
        if (this.props.customer.orders !== undefined && this.props.customer.orders[0].customerOrderItems !== undefined) {
            this.props.customer.orders[0].customerOrderItems.forEach(orderItem => {
                if (orderItem.sellPrice.product.id) {
                    orderItems.push(orderItem)
                }
            });
        }
        return (
            isMobile === false ?
            <div className="grid-x small-9 small-up-3 grid-padding-x grid-padding-y" style={{height: "fit-content"}}>
                <CategoryLine category={this.props.category}/>
                {commonProducts.map((commonProduct, index) =>
                    <ProductCard handleFavorite={this.handleFavorite}
                                 key={index} commonProduct={commonProduct} onChangeOrder={this.props.onChangeOrder}
                                 favorite={favorites.includes(commonProduct.id)}
                                 orderItems={this.getMatchedOrderItems(commonProduct)}/>
                )}
            </div>
                :
                <div className="grid-x small-up-1" style={{height: "fit-content"}}>
                    <CategoryLine category={this.props.category} categories={this.props.categories} onChangeCategory={this.props.onChangeCategory}/>
                    {commonProducts.map((commonProduct, index) =>
                        <ProductCard handleFavorite={this.handleFavorite}
                                     key={index} commonProduct={commonProduct} onChangeOrder={this.props.onChangeOrder}
                                     favorite={favorites.includes(commonProduct.id)}
                                     orderItems={this.getMatchedOrderItems(commonProduct)}/>
                    )}
                </div>
        )
    }
}

export default CategoryProducts;