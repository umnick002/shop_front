import React from "react";

class OSMSearch extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            input: '',
            addresses: [],
        };
        this.setAddresses = this.setAddresses.bind(this);
    }

    setAddresses(data) {
        this.setState({
            addresses: data
        });
    }

    onChangeAddress(input) {
        if (input !== this.state.input) {
            OSMSearch.showAddresses();
            this.setState({
                input: input
            });
            if (input.length > 3) {
                this.checkAddress(input, this.setAddresses);
            }
        }
    }

    static showAddresses() {
        if (document.querySelector('.addresses-container').classList.contains('hide')) {
            document.querySelector('.addresses-container').classList.remove('hide');
        }
    }

    static hideAddresses() {
        if (!document.querySelector('.addresses-container').classList.contains('hide')) {
            document.querySelector('.addresses-container').classList.add('hide');
        }
    }

    checkAddress(address, callback) {
        fetch('https://nominatim.openstreetmap.org/search?format=json&accept-language=ru-RU&addressdetails=1&countrycodes=BY&limit=5&q=' +
            encodeURIComponent(address)
        ).then(response => {
            return response.json();
        }).then(data => {
            if (Object.entries(data).length > 0) {
                OSMSearch.unMarshallAddress(data[0]);
                callback(data);
            }
        });
    }

    chooseAddress(address) {
        this.props.onChangeAddress(address);
        this.setState({
            input: OSMSearch.buildAddress(address)
        });
        OSMSearch.hideAddresses();
        this.isAvailable(address);
    }

    isAvailable(address) {
        fetch(`${host}/address/check?lon=${address.lon}&lat=${address.lat}`
        ).then(response => {
            return response.json();
        }).then(data => {
            if (Object.entries(data).length === 0) {
                document.querySelector('#address-search-error').classList.add('grid-x');
                document.querySelector('#address-search').classList.add('invalid-input');
            } else {
                document.querySelector('#address-search-error').classList.remove('grid-x');
                document.querySelector('#address-search').classList.remove('invalid-input');
            }
        });
    }

    static buildAddress(item) {
        let address = [];
        if (item.city) {
            address.push(item.city)
        }
        if (item.street) {
            address.push(item.street)
        }
        if (item.house) {
            address.push(item.house)
        }
        if (item.frame) {
            address.push('к' + item.frame)
        }
        if (item.building) {
            address.push('с' + item.building)
        }
        return address.join(' ');
    }

    componentDidMount() {
        document.body.addEventListener('mousedown', function (e) {
            if (document.querySelector('.addresses-container') !== null &&
                !document.querySelector('.addresses-container').classList.contains('hide') &&
                document.querySelector('#address-search:focus') !== null &&
                !(e.target.classList.contains('addresses-container') || e.target.classList.contains('address-item'))) {
                OSMSearch.hideAddresses();
            }
        })
    }

    onInputChange(field, value) {
        let address = {...this.props.address};
        address[field] = value;
        this.props.onChangeAddress(address);
    }

    onHouseChanged() {
        const address = this.props.address;
        this.checkAddress(OSMSearch.buildAddress(address), (data) => {
            if (data.length > 0) {
                for (let i = 0; i < data.length; i++) {
                    let item = data[i];
                    if (address.city === item.city &&
                        address.street === item.street &&
                        item.house
                    ) {
                        this.props.onChangeAddress(item);
                        return;
                    }
                }
            }
        })
    }

    static unMarshallAddress(address, isCommonFormat) {
        let addr = isCommonFormat === true ? address : address.address;
        [address.house, address.frame, address.building] = OSMSearch.houseComponents(addr);
        address.street = addr.road;
        if (address.city === undefined || address.city.length === 0) {
            address.city = addr.town !== undefined ? addr.town : addr.city;
        }
        if (addr.isFlat === undefined) {
            address.isFlat = true;
        } else {
            address.isFlat = addr.isFlat;
        }
    }

    static houseComponents(address) {
        let houseNumber = '';
        let frame = '';
        let building = '';
        let components = address.house_number !== undefined ? address.house_number.split(' ') : {};
        for (let i = 0; i < components.length; i++) {
            let component = components[i];
            if (component.indexOf('с') !== -1) {
                building = component.replace('с', '');
            } else if (component.indexOf('к') !== -1) {
                frame = component.replace('к', '');
            } else {
                houseNumber = component;
            }
        }
        return [houseNumber, frame, building];
    }

    addAddress() {
        let address = this.props.address;
        let isValid = true;
        if (address.house === undefined || address.house.length === 0) {
            document.querySelector('#house').classList.add('required');
            isValid = false;
        } else {
            document.querySelector('#house').classList.remove('required');
        }
        if ((address.isFlat === undefined || address.isFlat === true) && (address.flat === undefined || address.flat === '')) {
            document.querySelector('#flat').classList.add('required');
            isValid = false;
        } else {
            document.querySelector('#flat').classList.remove('required');
        }
        if (!isValid) {
            return
        }
        if (this.props.addressId !== undefined) {
            address.id = this.props.addressId;
        }
        OSMSearch.addDefaultFields(address);
        fetch(host + '/address', {
            method: address.id !== undefined ? 'PUT' : 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(address)
        })
        .then(response => {
            return response.json();
        })
        .then(data => {
            this.props.onAddAddress(data, this.props.addressId)
        })
    }

    static addDefaultFields(address) {
        if (address.floor === undefined) {
            address.floor = ''
        }
        if (address.frame === undefined) {
            address.frame = ''
        }
        if (address.building === undefined) {
            address.building = ''
        }
        if (address.entrance === undefined) {
            address.entrance = ''
        }
        if (address.flat === undefined) {
            address.flat = ''
        }
    }

    render() {
        let addresses = [];
        let addressesStr =[];
        this.state.addresses.forEach((item, index) => {
            let address = OSMSearch.buildAddress(item);
            if (address.length > 0 && !addressesStr.includes(address)) {
                addressesStr.push(address);
                addresses.push(
                    <div key={index} className='grid-x small-12 address-item'
                    onClick={() => this.chooseAddress(item)}>
                        {address}
                    </div>
                )
            }
        });
        const address = this.props.address || {};
        return (
            <div className="grid-x small-12">
                <div className='grid-x small-12 osm-search'>
                    <input id='address-search' type='text' placeholder={'Например, Полоцк Октябрьская улица 25'}
                           value={this.state.input}
                           onChange={(e) => this.onChangeAddress(e.target.value)}
                           onFocus={OSMSearch.showAddresses}
                    />
                    <span id='address-search-error' className="form-error">
                        Доставка осуществляется в пределах Полоцка и Новополоцка!
                    </span>
                    <div className='grid-y small-12 addresses-container'>
                        {addresses}
                    </div>
                </div>
                <form className={address !== undefined && Object.entries(address).length > 0 ?
                    'grid-x small-12 address-details' : 'grid-x small-12 address-details hide'}
                      onSubmit={(e) => {e.preventDefault();this.addAddress()}}>
                    <div className="grid-y small-12">
                        <label>Город<span className={'required'}>*</span></label>
                        <input id='city' type={'text'} value={address.city || ''} readOnly/>
                    </div>
                    <div className='grid-y small-12'>
                        <label>Улица<span className={'required'}>*</span></label>
                        <input id='street' type={'text'} value={address.street || ''} readOnly/>
                    </div>
                    <div className='grid-x small-12 align-justify'>
                        <div className='grid-y small-3'>
                            <label>Дом<span className={'required'}>*</span></label>
                            <input id='house' type={'text'} value={address.house || ''}
                            onChange={(e) => this.onInputChange('house', e.target.value)}
                            onBlur={() => this.onHouseChanged()}
                            />
                        </div>
                        <div className='grid-y small-3'>
                            <label>Корпус</label>
                            <input id='frame' type={'text'} value={address.frame || ''}
                                   onChange={(e) => this.onInputChange('frame', e.target.value)}
                                   onBlur={() => this.onHouseChanged()}
                            />
                        </div>
                        <div className='grid-y small-3'>
                            <label>Строение</label>
                            <input id='building' type={'text'} value={address.building || ''}
                                   onChange={(e) => this.onInputChange('building', e.target.value)}
                                   onBlur={() => this.onHouseChanged()}
                            />
                        </div>
                    </div>
                    <div className='grid-x small-12 align-justify'>
                        <div className='grid-y small-3'>
                            <label>Подъезд</label>
                            <input id='entrance' type={'text'} value={address.entrance || ''}
                                   onChange={(e) => this.onInputChange('entrance', e.target.value)}
                                   disabled={address.isFlat === false}
                            />
                        </div>
                        <div className='grid-y small-3'>
                            <label>Этаж</label>
                            <input id='floor' type={'text'} value={address.floor || ''}
                                   onChange={(e) => this.onInputChange('floor', e.target.value)}
                                   disabled={address.isFlat === false}
                            />
                        </div>
                        <div className='grid-y small-3'>
                            <label>Квартира{address.isFlat === true ? <span className={'required'}>*</span> : null}</label>
                            <input id='flat' type={'text'} value={address.flat || ''}
                                   onChange={(e) => this.onInputChange('flat', e.target.value)}
                                   disabled={address.isFlat === false}
                            />
                        </div>
                    </div>
                    <div className='grid-x small-12 align-middle'>
                        <input type='checkbox' checked={!address.isFlat || false}
                               onChange={(e) => this.onInputChange('isFlat', !e.target.checked)}
                               style={{margin: '0 .4rem 0 0', transform: 'scale(1.4)'}}/>
                        <span style={{fontSize: '0.9rem', color: '#333333'}}>Доставка в частный дом</span>
                    </div>
                    <div className='grid-x small-12 align-right'>
                        <button type='submit' className='button'
                                style={{marginBottom: '0'}}>Подтвердить адрес</button>
                    </div>
                </form>
            </div>
        )
    }
}

export default OSMSearch;