import React from "react";
import {Map, Marker, TileLayer} from "react-leaflet";
import OSMSearch from "./OSMSearch";

class OSMMap extends React.Component {

    constructor(props) {
        super(props);
    }

    reverseAddress(coords) {
        if (document.querySelector('#address-search')) {
            fetch('https://nominatim.openstreetmap.org/reverse?zoom=18&format=json&accept-language=ru-RU&addressdetails=1&countrycodes=BY&limit=5&lon=' +
                coords.lng + '&lat=' + coords.lat
            ).then(response => {
                return response.json();
            }).then(data => {
                data.lon = coords.lng;
                data.lat = coords.lat;
                OSMSearch.unMarshallAddress(data);
                this.props.onChangeAddress(data);
            });
        }
    }

    render() {
        const address = this.props.address;
        const marker = Object.entries(address).length > 0 ? (
            <Marker position={[address.lat, address.lon]}>
            </Marker>
        ) : null;
        return (
            <Map center={Object.entries(address).length > 0 ? [address.lat, address.lon] : [55.49, 28.78]}
                 zoom={Object.entries(address).length > 0 ? 17 : 13}
                 onClick={(e) => this.reverseAddress(e.latlng)}>
                <TileLayer
                    url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                    attribution="&copy; <a href=&quot;http://osm.org/copyright&quot;>OpenStreetMap</a> contributors"
                />
                {marker}
            </Map>
        )
    }
}

export default OSMMap;