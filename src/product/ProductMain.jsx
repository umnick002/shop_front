import React from "react";
import ProductVolumes from "./ProductVolumes";
import ProductPrice from "./ProductPrice";
import ProductToCart from "./ProductToCart";
import Stars from "./Stars";

class ProductMain extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            active: -1
        };
        this.handleClickProduct = this.handleClickProduct.bind(this);
    }

    handleClickProduct(i) {
        this.setState({
            active: i
        })
    }

    render() {
        const products = this.props.products;
        if (Object.entries(products).length === 0) {
            products[0] = {id: -1, sellPrices: []}
        }
        let active;
        if (this.state.active !== -1) {
            active = this.state.active;
        } else if (products.length > 0) {
            active = 0;
        } else {
            active = -1;
        }
        let orderItem = {};
        this.props.orderItems.forEach(oi => {
            if (oi.sellPrice.product.id === products[active].id) {
                orderItem = oi;
            }
        });
        return (
            isMobile === false ?
            <div className="grid-y small-4 small-offset-1">
                <div className="grid-x small-12 align-justify" style={{height: "fit-content"}}>
                    {
                        products[active].number !== undefined ?
                            <div className="grid-x text-right align-middle" style={{fontSize: "0.8rem", color: "#777"}}>
                                Артикул: {products[active].number}
                            </div>
                            : ""
                    }
                    <div className="grid-x small-8 rating-block" style={{justifyContent: 'flex-end'}}>
                        <Stars rating={Math.ceil(this.props.rating)} edit={false}/>
                        <div style={{fontSize: "0.9rem", color: "#777"}}>({this.props.comments.length})</div>
                    </div>
                </div>
                <div className="grid-y callout secondary" style={{height: "fit-content"}}>
                    <h5><b>{this.props.name}</b></h5>
                    <div style={{marginTop: "0.8rem"}}>
                        <div><b>Выберите вес:</b></div>
                        <ProductVolumes handleClickProduct={this.handleClickProduct} products={products} active={active}/>
                    </div>
                    <div>
                        {products[active].reserved ?
                            <div className="grid-x" style={{marginTop: "1rem"}}>
                                <ProductPrice product={products[active]}/>
                                <div className="grid-x small-6 align-right product-to-cart">
                                    <ProductToCart activeProduct={products[active]} orderItem={orderItem}
                                                   onChangeOrder={this.props.onChangeOrder}
                                                   content={products[active].commonProduct !== undefined ? products[active].commonProduct.contents[0] : ""}/>
                                </div>
                            </div>
                            :
                            <div style={{color: '#cc4b37', marginTop: '0.4rem', fontWeight: '600'}}>
                                Нет в наличии
                            </div>
                        }
                    </div>
                </div>
            </div>
                :
            <div className="grid-y small-12">
                <div className="grid-y secondary" style={{height: "fit-content"}}>
                    <div style={{marginTop: "0.8rem"}}>
                        <div><b>Выберите вес:</b></div>
                        <ProductVolumes handleClickProduct={this.handleClickProduct} products={products} active={active}/>
                    </div>
                    <div className={isMobile === true ? 'bottom-border' : ''} style={isMobile === true ? {paddingBottom: '.5rem'} : {}}>
                        {products[active].reserved ?
                            <div className="grid-x" style={{marginTop: "1rem"}}>
                                <ProductPrice product={products[active]}/>
                                <div className="grid-x small-6 align-right product-to-cart">
                                    <ProductToCart activeProduct={products[active]} orderItem={orderItem}
                                                   onChangeOrder={this.props.onChangeOrder}
                                                   content={products[active].commonProduct !== undefined ? products[active].commonProduct.contents[0] : ""}/>
                                </div>
                            </div>
                            :
                            <div style={{color: '#cc4b37', marginTop: '0.4rem', fontWeight: '600'}}>
                                Нет в наличии
                            </div>
                        }
                    </div>
                </div>
            </div>
        );
    }
}

export default ProductMain;