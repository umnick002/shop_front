import React from "react";

class Stars extends React.Component {

    setRating(rating) {
        if (this.props.edit) {
            this.props.onRatingChange(rating + 1)
        }
    }

    showRating(rating) {
        if (this.props.edit) {
            document.querySelectorAll(".add-comment polygon").forEach((star, index) => {
                if (rating >= index) {
                    star.style.fill = "#FFC709";
                }
            })
        }
    }

    clearRating() {
        if (this.props.edit) {
            document.querySelectorAll(".add-comment polygon").forEach((star, index) => {
                if (index >= this.props.rating) {
                    star.style.fill = "#E8E8E8";
                }
            })
        }
    }

    render() {
        let stars = [];
        for (let i = 0; i < 5; i++) {
            stars.push(
                <div className={"star grid-x align-center"} key={i}
                     onMouseOver={() => this.showRating(i)}
                     onMouseOut={() => this.clearRating()}
                     onClick={() => this.setRating(i)}>
                    <svg xmlns="http://www.w3.org/2000/svg" width="20" height="18" viewBox="0 0 40 37">
                        <polygon fill={i < this.props.rating ? "#FFC709" : "#E8E8E8"}
                                 points="272 30 260.244 36.18 262.489 23.09 252.979 13.82 266.122 11.91 272 0 277.878 11.91 291.021 13.82 281.511 23.09 283.756 36.18"
                                 transform="translate(-252)"/>
                    </svg>
                </div>
            );
        }
        return (
            <div className="grid-x rating-block-rating" style={{flexWrap: "nowrap"}} data-rating>
                {stars}
            </div>
        )
    }
}

export default Stars;