import React from "react";
import Stars from "./Stars";
import ProductProperties from "./ProductProperties";

class ProductComment extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            rating: 0,
            comment: ''
        };
        this.onRatingChange = this.onRatingChange.bind(this);
    }

    onRatingChange(rating) {
        this.setState({
            rating: rating
        })
    }

    onChangeComment(comment) {
        this.setState({
            comment: comment
        })
    }

    onCommentAdd(e) {
        e.preventDefault();
        if (this.state.rating > 0) {
            fetch(host + window.location.pathname + '/comments', {
                method: 'POST',
                body: JSON.stringify(this.state),
                headers: {
                    'Content-Type': 'application/json;charset=UTF-8'
                },
            }).then(response => {
                return response.json();
            }).then(data => {
                this.props.onCommentChange(data);
            });
        }
    }

    componentDidUpdate(prevProps) {
        if ((Object.entries(prevProps.comments).length === 0 || Object.entries(prevProps.customer).length === 1)
            && Object.entries(this.props.comments).length > 0) {
            for (let i in this.props.comments) {
                let item = this.props.comments[i];
                if (item.customer.id === this.props.customer.id) {
                    this.setState({
                        comment: item.comment,
                        rating: item.rating
                    });
                    break;
                }
            }
        }
    }

    render() {
        return (
            isMobile === false ?
            <div className="grid-y comment-block">
                <div>
                    <form className="grid-y add-comment" onSubmit={(e) => this.onCommentAdd(e)}>
                        <div className="grid-x">
                            <div className="small-1">
                                {this.props.customer.avatar ?
                                    <img src={staticHost + this.props.customer.avatar}/>
                                    :
                                    <i className="far fa-user-circle"/>
                                }
                            </div>
                            <div className="small-6">
                                <textarea placeholder="Написать отзыв" value={this.state.comment}
                                          onChange={(e) => this.onChangeComment(e.target.value)}/>
                            </div>
                            <div className="small-offset-1 small-4">
                                <div className="small-title">Оцените качество товара</div>
                                <Stars rating={Math.ceil(this.state.rating)} edit={true} onRatingChange={this.onRatingChange}/>
                            </div>
                        </div>
                        <div className="grid-x small-offset-1 submit-button">
                            <button type="submit" className="grid-x small-3 button">
                                Отправить
                            </button>
                        </div>
                    </form>
                    <hr/>
                </div>
                {this.props.comments.map((item) =>
                    <div key={item.id}>
                        <div className="grid-x small-12">
                            <div className="small-1">
                                {item.customer.avatar ?
                                    <img src={staticHost + item.customer.avatar}/>
                                    :
                                    <i className="far fa-user-circle"/>
                                }
                            </div>
                            <div className="grid-y small-11">
                                <div className="grid-x align-justify">
                                    <div>
                                        <Stars rating={Math.ceil(item.rating)} edit={false}/>
                                    </div>
                                    <div style={{color: "#777", fontSize: "0.8rem"}}>
                                        {new Date(item.created).toLocaleDateString('ru-ru',
                                            { weekday: 'short', year: 'numeric', month: 'long', day: 'numeric', hour: 'numeric', minute: 'numeric' })}
                                     </div>
                                </div>
                                <div className="grid-x">
                                    {item.comment}
                                </div>
                            </div>
                        </div>
                        <hr/>
                    </div>
                )}
            </div>
                :
            <div className="grid-y small-12 comment-block">
                <div style={{marginTop: ".5rem"}}>
                    <form className="grid-y add-comment" onSubmit={(e) => this.onCommentAdd(e)}>
                        <div className="grid-x">
                            <div className="small-2">
                                {this.props.customer.avatar ?
                                    <img src={staticHost + this.props.customer.avatar}/>
                                    :
                                    <i className="far fa-user-circle"/>
                                }
                            </div>
                            <div className="small-10">
                                <textarea placeholder="Написать отзыв" value={this.state.comment}
                                          onChange={(e) => this.onChangeComment(e.target.value)}/>
                            </div>
                        </div>
                        <div className="small-4">
                            <div className="small-title">Оцените качество товара</div>
                            <Stars rating={Math.ceil(this.state.rating)} edit={true} onRatingChange={this.onRatingChange}/>
                        </div>
                        <div className="grid-x submit-button">
                            <button type="submit" className="grid-x button">
                                Отправить
                            </button>
                        </div>
                    </form>
                    <hr/>
                </div>
                {this.props.comments.map((item) =>
                    <div key={item.id} style={{marginTop: ".5rem"}}>
                        <div className="grid-x small-12">
                            <div className="small-2">
                                {item.customer.avatar ?
                                    <img src={staticHost + item.customer.avatar}/>
                                    :
                                    <i className="far fa-user-circle"/>
                                }
                            </div>
                            <div className="grid-y small-10">
                                <div>
                                    <Stars rating={Math.ceil(item.rating)} edit={false}/>
                                </div>
                                <div style={{color: "#777", fontSize: "0.8rem"}}>
                                    {new Date(item.created).toLocaleDateString('ru-ru',
                                        { weekday: 'short', year: 'numeric', month: 'long', day: 'numeric', hour: 'numeric', minute: 'numeric' })}
                                </div>
                            </div>
                            <div className="grid-x">
                                {item.comment}
                            </div>
                        </div>
                        <hr/>
                    </div>
                )}
            </div>
        )
    }
}

class ProductData extends React.Component {
    render() {
        return (
            isMobile === false ?
            <div className="grid-x small-8 desktop" style={{paddingRight: "2rem"}}>
                <ul className="tabs" data-tabs id="product-tabs">
                    <li className="tabs-title is-active"><a href="#panel1" aria-selected="true">Описание</a></li>
                    <li className="tabs-title" style={{marginLeft: "2px"}}><a data-tabs-target="panel2" href="#panel2">
                        Отзывы ({(this.props.comments && this.props.comments.length) || 0})</a></li>
                </ul>
                <div className="tabs-content small-12" data-tabs-content="product-tabs" style={{minHeight: "300px", maxHeight: "300px", overflowY: "auto"}}>
                    <div className="tabs-panel is-active" id="panel1">
                        {this.props.description}
                    </div>
                    <div className="tabs-panel" id="panel2">
                        <ProductComment customer={this.props.customer} comments={this.props.comments}
                                        onCommentChange={this.props.onCommentChange}/>
                    </div>
                </div>
            </div>
                :
            <div className="grid-x small-12 mobile">
                <ul className="tabs small-12 bottom-border" data-tabs id="product-tabs">
                    <li className="tabs-title is-active"><a href="#panel1" aria-selected="true">Описание</a></li>
                    <li className="tabs-title" style={{marginLeft: "2px"}}><a data-tabs-target="panel2" href="#panel2">
                        Отзывы ({(this.props.comments && this.props.comments.length) || 0})</a></li>
                </ul>
                <div className="small-12" data-tabs-content="product-tabs">
                    <div className="tabs-panel is-active" id="panel1">
                        <ProductProperties properties={this.props.properties}/>
                        {this.props.description}
                    </div>
                    <div className="tabs-panel" id="panel2">
                        <ProductComment customer={this.props.customer} comments={this.props.comments}
                                        onCommentChange={this.props.onCommentChange}/>
                    </div>
                </div>
            </div>
        )
    }
}

export default ProductData;