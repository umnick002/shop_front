import React from "react";

class ProductProperties extends React.Component {
    render() {
        const properties = this.props.properties;
        return (
            <div className={"grid-y small-4 " + (isMobile === true ? 'bottom-border' : '')} style={{margin: ".5rem 0"}}>
                {isMobile === false ? <div><h5 style={{color: "#0db14b"}}>Характеристики</h5></div> : null}
                <div style={isMobile === true ? {padding: ".5rem 0"} : {}}>
                    {properties.map((item) =>
                        <div className="grid-x" key={item.id}>
                            <div><b>{item.key.name}:</b></div>
                            <div style={{padding: "0 .5rem", fontStyle: "italic"}}><span>{item.value}</span></div>
                        </div>
                    )}
                </div>
            </div>
        );
    }
}

export default ProductProperties;