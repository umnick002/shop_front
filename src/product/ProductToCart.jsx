import React from "react";

class ProductToCart extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            count: -1
        };
        this.onChangeCount = this.onChangeCount.bind(this);
    }

    onChangeCount(count) {
        this.setState({
            count: count
        })
    }

    handleSetCount(count) {
        count = parseInt(count) || 0;
        if (count < 0) {
            count = 0;
        } else if (this.state.count === 0) {
            this.onChangeCount(count);
        }
        this.props.onChangeOrder(this.props.activeProduct.id, count);
    }

    componentDidUpdate(prevProps) {
        if (prevProps.orderItem !== undefined && this.props.orderItem !== undefined &&
            prevProps.orderItem.count !== this.props.orderItem.count) {
            this.onChangeCount(this.props.orderItem.count)
        }
    }

    render() {
        let isNoneCount = this.props.orderItem === undefined || Object.entries(this.props.orderItem).length === 0
            || this.props.orderItem.count === 0;
        let value;
        if (!isNoneCount) {
            if (this.state.count > 0) {
                value = this.state.count
            } else {
                value = this.props.orderItem.count
            }
        } else if (this.state.count > 0) {
            isNoneCount = false;
            value = this.state.count;
        }
        return (
            <div className="grid-x">
                <button type="button" className="button"
                        style={{display: !isNoneCount ? "none" : "inline-block"}}
                        onClick={() => this.handleSetCount(1)}>
                    <i className="fas fa-lg fa-cart-plus"/>
                </button>
                <div className="grid-x" style={{display: !isNoneCount ? "flex" : "none", flexWrap: "nowrap"}}>
                    <button type="button" className="button secondary minus" onClick={() => this.handleSetCount(this.props.orderItem.count - 1)}>
                        <i className="fas fa-minus"/>
                    </button>
                    <input onChange={(e) => this.onChangeCount(e.target.value.replace(/\D/g,''))}
                           onBlur={() => this.handleSetCount(this.state.count)}
                           onFocus={() => this.onChangeCount(value || 1)} value={value || 1}/>
                    <button type="button" className="button plus" onClick={() => this.handleSetCount(this.props.orderItem.count + 1)}>
                        <i className="fas fa-plus"/>
                    </button>
                </div>
            </div>
        )
    }
}

export default ProductToCart;