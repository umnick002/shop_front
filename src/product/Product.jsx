import React from "react";
import ProductContent from "./ProductContent";
import ProductMain from "./ProductMain";
import ProductData from "./ProductData";
import ProductProperties from "./ProductProperties";
import Stars from "./Stars";

class Product extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            contents: [],
            properties: [],
            products: [],
            rating: 0,
            favorite: false,
            comments: [],
            active: -1
        };
        this.handleFavorite = this.handleFavorite.bind(this);
        this.onCommentChange = this.onCommentChange.bind(this);
    }

    componentDidMount() {
        fetch(host + window.location.pathname).then(response => {
            return response.json();
        }).then(data => {
            console.log(data);
            this.setState(data);
            document.querySelector("body").hidden = false;
        });

        fetch(host + '/favorites/' + window.location.pathname.slice(window.location.pathname.lastIndexOf('/') + 1))
        .then(response => {
            return response.json();
        }).then(data => {
            this.setState({favorite: data});
        }).catch();

        fetch(host + window.location.pathname + '/comments')
        .then(response => {
            return response.json();
        })
        .then(data => {
            this.setState({
                comments: data
            })
        });
    }

    handleFavorite(isFavorite) {
        this.setState({
            favorite: isFavorite
        })
    }

    onCommentChange(comment) {
        let comments = this.state.comments;
        let found = false;
        for (let i in this.state.comments) {
            let item = this.state.comments[i];
            if (item.id === comment.id) {
                comments[i].rating = comment.rating;
                comments[i].comment = comment.comment;
                comments[i].created = comment.created;
                found = true;
                break;
            }
        }
        if (!found) {
            console.log(comment, this.props.customer);
            if (!comment.hasOwnProperty('customer')) {
                comment.customer = this.props.customer
            }
            comments.push(comment);
        }
        this.setState({
            comments: comments
        })
    }

    render() {
        const products = this.state.products;
        if (Object.entries(products).length === 0) {
            products[0] = {id: -1, sellPrices: []}
        }
        let active;
        if (this.state.active !== -1) {
            active = this.state.active;
        } else if (products.length > 0) {
            active = 0;
        } else {
            active = -1;
        }
        return (
            isMobile === false ?
            <div id="root" className="product" style={{maxWidth: "1000px", margin: "1rem 0"}}>
                <div className="align-spaced grid-x small-12 product-top">
                    <ProductContent contents={this.state.contents} favorite={this.state.favorite} handleFavorite={this.handleFavorite}/>
                    <ProductMain onChangeOrder={this.props.onChangeOrder} name={this.state.name}
                                 orderItems={this.props.customer.orders ? this.props.customer.orders[0].customerOrderItems : []} rating={this.state.rating}
                                 products={this.state.products} favorite={this.state.favorite} content={this.props.content}
                                 comments={this.state.comments}/>
                </div>
                <hr />
                <div className="grid-x small-12">
                    <ProductData customer={this.props.customer} description={this.state.description}
                                 comments={this.state.comments} onCommentChange={this.onCommentChange}/>
                    <ProductProperties properties={this.state.properties}/>
                </div>
            </div>
                :
            <div id="root" className="product" style={{maxWidth: "1000px", margin: "1rem .5rem"}}>
                <div className="align-spaced grid-x product-top">
                    <div className="grid-x small-12 align-justify" style={{height: "fit-content"}}>
                        {
                            products[active].number !== undefined ?
                                <div className="grid-x" style={{fontSize: "0.8rem", color: "#777"}}>
                                    Артикул: {products[active].number}
                                </div>
                                : ""
                        }
                        <div className="grid-x rating-block" style={{justifyContent: 'flex-end'}}>
                            <Stars rating={Math.ceil(this.state.rating)} edit={false}/>
                            <div style={{fontSize: "0.9rem", color: "#777"}}>({this.state.comments.length})</div>
                        </div>
                    </div>
                    <div className="grid-x small-12 align-justify product-name">
                        {this.state.name}
                    </div>
                    <ProductContent contents={this.state.contents} favorite={this.state.favorite} handleFavorite={this.handleFavorite}/>
                    <ProductMain onChangeOrder={this.props.onChangeOrder} name={this.state.name}
                                 orderItems={this.props.customer.orders ? this.props.customer.orders[0].customerOrderItems : []} rating={this.state.rating}
                                 products={this.state.products} favorite={this.state.favorite} content={this.props.content}
                                 comments={this.state.comments}/>
                    <div className="grid-x small-12" style={{marginTop: ".5rem"}}>
                        <ProductData customer={this.props.customer} description={this.state.description}
                                     comments={this.state.comments} onCommentChange={this.onCommentChange}
                                     properties={this.state.properties}/>
                    </div>
                </div>
            </div>
        );
    }
}

export default Product;