import React from "react";

class ProductContent extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            active: {}
        };
    }

    handleClick(item) {
        this.setState({
            active: item
        })
    }

    handleEnableFavorite(target) {
        if (this.props.favorite === false) {
            target.classList.remove('far');
            target.classList.add('fas');
        }
    }

    handleDisableFavorite(target) {
        if (this.props.favorite === false) {
            target.classList.remove('fas');
            target.classList.add('far');
        }
    }

    handleFavorite() {
        fetch(host + '/favorites/' + window.location.pathname.slice(window.location.pathname.lastIndexOf('/') + 1), {
            method: this.props.favorite ? 'DELETE' : 'POST'
        })
            .then((response) => {
                console.log(response);
                if (response.status === 200) {
                    this.props.handleFavorite(!this.props.favorite)
                } else if (response.status === 401) {
                    document.querySelector('#auth').classList.remove('hide');
                    document.querySelector('#auth').classList.add('grid-x');
                }
            });
    }

    render() {
        const contents = this.props.contents;
        let active;
        if (Object.entries(this.state.active).length !== 0) {
            active = this.state.active;
        } else if (contents.length > 0) {
            active = contents[0];
        } else {
            active = {};
        }
        return (
            isMobile === false ?
            <div className="cell grid-x grid-padding-x small-7 desktop">
                <div className="grid-y small-2 product-images">
                    {contents.map((item) =>
                        <img className="thumbnail" key={item.id} src={staticHost + item.file}
                             onClick={() => this.handleClick(item)}/>
                    )}
                </div>
                <div className="grid-x align-center small-10" style={{position: "relative"}}>
                    <button className="favorite-button" onMouseOver={(e) => this.handleEnableFavorite(e.target)}
                            onMouseOut={(e) => this.handleDisableFavorite(e.target)}
                            onClick={(e) => this.handleFavorite(e.target)}>
                        <i className={(this.props.favorite ? "fas" : "far") + " fa-lg fa-heart"}/>
                    </button>
                    <div className="grid-y align-center-middle active-photo-container">
                        <img data-toggle="active_photo" className="active-photo"
                             src={Object.entries(active).length > 0 ? (staticHost + active.file) : ""}/>
                    </div>
                    <div className="large reveal text-center" id="active_photo" data-reveal>
                        <img src={Object.entries(active).length > 0 ? (staticHost + active.file) : ""}/>
                        <button className="close-button" data-close="active_photo" aria-label="Close reveal" type="button">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                </div>
            </div>
                :
            <div className="cell grid-x grid-padding-x mobile">
                <div className="grid-x align-center small-12" style={{position: "relative"}}>
                    <button className="favorite-button" onMouseOver={(e) => this.handleEnableFavorite(e.target)}
                            onMouseOut={(e) => this.handleDisableFavorite(e.target)}
                            onClick={(e) => this.handleFavorite(e.target)}>
                        <i className={(this.props.favorite ? "fas" : "far") + " fa-lg fa-heart"}/>
                    </button>
                    <div className="grid-y align-center-middle active-photo-container">
                        <img className="active-photo"
                             src={Object.entries(active).length > 0 ? (staticHost + active.file) : ""}/>
                    </div>
                </div>
                <div className="grid-x scrollable">
                    <div className="grid-x">
                    {contents.map((item) =>
                        <div className="grid-y align-center-middle photo-container" key={item.id}>
                            <img src={staticHost + item.file}
                                 onClick={() => this.handleClick(item)}/>
                        </div>
                    )}
                    </div>
                </div>
            </div>
        );
    }
}

export default ProductContent;