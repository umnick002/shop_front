import React from "react";

class ProductVolumes extends React.Component {

    handleClickProduct(i) {
        this.props.handleClickProduct(i);
    }

    render() {
        const products = this.props.products;
        const active = this.props.active;
        return (
            <div className={"small-12 grid-x grid-margin-x align-center select-div " + (isMobile === false ? "small-up-4" : "small-up-3")}
                 style={{fontSize: "0.8rem", userSelect: "none"}}>
                {products.map((product, index) =>
                    <div className={active === index ? "cell text-center selected-div" : "cell text-center"}
                         onClick={() => this.handleClickProduct(index)} key={product.id} id={product.id}
                         style={products.length > 1 ? {cursor: "pointer"} : {cursor: "default"}}>
                        <div className={'volume'}>
                            {parseFloat(product.packVolume).toFixed(2)} {product.packVolumeUnit}
                        </div>
                    </div>
                )}
            </div>
        )
    }
}

export default ProductVolumes;