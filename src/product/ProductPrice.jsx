import React from "react";

class ProductPrice extends React.Component {
    render() {
        const product = this.props.product;
        const sellPrice = product.sellPrices ? product.sellPrices[0] : undefined;
        return (
            <div className="grid-y small-6" style={{lineHeight: "1.6rem"}}>
                {sellPrice !== undefined && sellPrice.pricePerUnit !== undefined ?
                    <div>
                        <div style={{fontSize: "0.9rem"}}>
                            {parseFloat(sellPrice.pricePerUnit).toFixed(2)} р/{product.packVolumeUnit}
                        </div>
                        <div style={{fontSize: "1.2rem"}}>
                            <b>{parseFloat(sellPrice.price).toFixed(2)} р/шт</b>
                        </div>
                    </div>
                    : ""}
            </div>
        )
    }
}

export default ProductPrice;