import React from "react";
import InputMask from 'react-input-mask';

class CustomerPhone extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            showCodeBlock: false,
            phone: '',
            validPhone: false,
            verifiedPhone: false,
        }
    }

    sendVerifyingCode() {
        fetch(`${host}/phone/code?phone=${this.state.phone.replace(/[^0-9]/g, '')}`, {
            method: 'POST'
        }).then(data => data.json()).then(data => {
            console.log(data);
            if (data.verified === false) {
                this.setState({
                    showCodeBlock: true
                })
            } else {
                this.setState({
                    showCodeBlock: false
                })
            }
        })
    }

    verifyPhone() {
        const code = document.querySelector('.code-input').value;
        fetch(`${host}/phone/verify?phone=${this.state.phone.replace(/[^0-9]/g, '')}&code=${code}`, {
            method: 'POST'
        }).then(data => data.json()).then(data => {
            if (Object.entries(data).length > 0) {
                this.setState({
                    showCodeBlock: false
                });
                this.props.onAddPhone(data);
            } else {
                document.querySelector('.code-input-error').classList.add('grid-x');
                document.querySelector('.code-input').classList.add('invalid-input');
            }
        })
    }

    onChangePhone(phone) {
        this.setState({
            phone: phone,
            validPhone: phone.replace(/[^0-9]/g, '').length === 10
        })
    }

    render() {
        return (
        <div>
            {this.props.additional === false ?
                <label className="grid-x">Телефон<span className={'required'}>*</span></label>
                :
                <label className="grid-x">Дополнительный номер</label>
            }
            <div className="grid-x">
                <InputMask className="small-12 phone-input" type={"tel"}
                           mask={'(099) 999-99-99'}
                           value={this.state.phone}
                           placeholder={'(0__) ___-__-__'}
                           onChange={(e) => this.onChangePhone(e.target.value)}/>
            </div>
            {this.state.verifiedPhone === false ?
                <div className='grid-x small-12 align-justify code'>
                    <button
                        className={'grid-x small-4 secondary button ' + (this.state.validPhone !== true ? 'disabled' : '')}
                        type='button' onClick={() => this.sendVerifyingCode()}>Отправить код
                    </button>
                    <div className={'grid-x small-6 align-justify ' + (this.state.showCodeBlock !== true ? 'invisible' : '')}>
                        <div className='grid-y small-6'>
                            <div className='grid-x code-container'>
                                <input className={'small-12 code-input'} type='text'/>
                                <span className="small-12 code-input-error form-error">
                                    Неправильный код!
                                </span>
                            </div>
                        </div>
                        <button className={'small-5 button success'} type={'button'}
                                onClick={() => this.verifyPhone()}>Подтвердить
                        </button>
                    </div>
                </div>
                : null
            }
        </div>
        )
    }
}

export default CustomerPhone;