import React from "react";
import OSMMap from "../map/OSMMap";
import OSMSearch from "../map/OSMSearch";
import DeliveryDate from "../utils/DeliveryDate";
import DeliveryTime from "../utils/DeliveryTime";
import CustomerPhone from "../customer/CustomerPhone";

class OrderDelivery extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            address: {},
            addresses: [],
            custom: false,
            addressId: undefined,
            selectedDate: null,
            availableDates: [],
            availableTimes: [],
            selectedTimes: [],
            name: '',
            mainPhone: {},
            phones: [],
            addPhone: false
        };
        this.onChangeAddress = this.onChangeAddress.bind(this);
        this.onAddAddress = this.onAddAddress.bind(this);
        this.onSelectedDateChange = this.onSelectedDateChange.bind(this);
        this.marshallAddress = this.marshallAddress.bind(this);
        this.onChangeSelectedTimes = this.onChangeSelectedTimes.bind(this);
        this.onAddPhone = this.onAddPhone.bind(this);
        this.onAddPhone = this.onAddPhone.bind(this);
    }

    toNextStep() {
        if (Object.entries(this.state.address).length === 0) {
            return
        } else if (this.state.selectedDate === null) {
            return
        } else if (this.state.selectedTimes.length === 0) {
            return
        }
    }

    static back() {
        window.location = '/order/items';
    }

    componentDidMount() {
        fetch(host + '/address').then(response => {
            return response.json();
        }).then(data => {
            this.setState({
                addresses: data
            })
        });
        fetch(host + '/phones?verified=true').then(response => {
            return response.json();
        }).then(data => {
            if (data.length > 0) {
                this.setState({
                    phones: data,
                    mainPhone: data[0]
                })
            }
        });
    }

    componentDidUpdate(prevProps, prevState) {
        if (this.state.name === '' && prevProps.customer.name !== undefined) {
            this.setState({
                name: prevProps.customer.name
            })
        }
        if (Object.entries(this.state.address).length === 0 && this.state.custom === false &&
            this.state.addresses !== undefined && this.state.addresses.length > 0) {
            this.setState({
                address: this.state.addresses[0]
            })
        }
        if (prevState.address !== this.state.address) {
            this.setState({
                selectedDate: null,
                availableDates: [],
                availableTimes: [],
                selectedTimes: []
            });
            this.fetchAvailableDates(this.state.address.id);
        }
    }

    onChangeAddress(address) {
        if (address.isFlat === undefined && this.state.address.isFlat === undefined) {
            address.isFlat = true;
        }
        this.setState({
            address: address,
            custom: true
        });
    }

    onAddAddress(address, oldAddressId) {
        let addresses = this.state.addresses;
        let isNew = true;
        if (addresses !== undefined) {
            for (let i = 0; i < addresses.length; i++) {
                if (addresses[i].id === address.id || (oldAddressId !== undefined && addresses[i].id === oldAddressId)) {
                    isNew = false;
                    addresses.splice(i, 1);
                    break
                }
            }
        } else {
            addresses = [];
        }
        addresses.push(address);
        this.setState({
            addresses: addresses,
            custom: false,
            address: address
        });
    }

    clearAddress() {
        this.setState({
            address: this.state.addresses !== undefined && this.state.addresses.length > 0 ?
                this.state.addresses[0] : {},
            custom: false
        })
    }

    addAddress() {
        this.setState({
            address: {},
            custom: true
        })
    }

    static buildAddress(item) {
        let address = [];
        if (item.city) {
            address.push(item.city)
        }
        if (item.street) {
            address.push(item.street)
        }
        if (item.house) {
            address.push('д. ' + item.house)
        }
        if (item.frame) {
            address.push('к. ' + item.frame)
        }
        if (item.building) {
            address.push('стр. ' + item.building)
        }
        if (item.flat) {
            address.push('кв. ' + item.flat)
        }
        return address.join(', ');
    }

    makeActiveAddress(address, e) {
        if (e === undefined || !e.target.classList.contains('fa-pencil-alt')) {
            this.setState({
                address: address
            });
        } else {
            this.marshallAddress(address);
        }
    }

    fetchAvailableDates(addressId) {
        if (addressId !== undefined) {
            fetch(`${host}/delivery/days?address_id=${addressId}`)
                .then(data => data.json())
                .then(data => {
                    if (data.length > 0) {
                        let days = [];
                        for (let day of data) {
                            days.push(new Date(day));
                        }
                        this.setState({
                            availableDates: days
                        });
                    }
                });
        } else {
            this.setState({
                availableDates: []
            });
        }
    }

    fetchAvailableTimes(addressId, dayStr) {
        fetch(`${host}/delivery/times?address_id=${addressId}&day=${dayStr}`)
        .then(data => data.json())
        .then(data => {
            this.setState({
                availableTimes: data
            })
        })
    }

    onChangeSelectedTimes(time) {
        let times = this.state.selectedTimes;
        for (let i in times) {
            if (times[i].id === time.id) {
                times.splice(i, 1);
                this.setState({
                    selectedTimes: times
                });
                return;
            }
        }
        times.push(time);
        this.setState({
            selectedTimes: times
        });
    }

    marshallAddress(addr) {
        this.setState({
            address: addr,
            addressId: addr.id,
            custom: true
        })
    }

    onSelectedDateChange(date) {
        if (this.state.selectedDate === null || this.state.selectedDate !== date) {
            this.setState({
                selectedDate: date,
                selectedTimes: []
            });
            this.fetchAvailableTimes(this.state.address.id, date.toISOString().split('T')[0]);
        }
    }

    onAddPhone(phone) {
        let phones = this.state.phones;
        phones.unshift(phone);
        this.setState({
            phones: phones,
            mainPhone: phone,
            addPhone: false
        });
    }

    updateName() {
        fetch(`${host}/customer`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json;charset=UTF-8'
            },
            body: JSON.stringify({name: this.state.name})
        }).then(data => data.json()).then(data => {
            console.log(data)
        });
    }

    onChangeMainPhone(id) {
        if (id !== -1) {
            this.setState({
                mainPhone: this.getPhoneById(id)
            });
        } else {
            this.setState({
                addPhone: true
            })
        }
    }

    getPhoneById(id) {
        for (let phone of this.state.phones) {
            if (phone.id === id) {
                return phone;
            }
        }
        return null;
    }

    maskPhone(phone) {
        return `(${phone.slice(0, 3)}) ${phone.slice(3, 6)}-${phone.slice(6, 8)}-${phone.slice(8, 10)}`
    }

    render() {
        let phones = this.state.phones || [];
        if (phones.length > 0 && this.getPhoneById(-1) === null) {
            phones.push({id: -1, phone: 'Добавить дополнительный номер'})
        }
        return (
            <div className="grid-x small-12 basket-delivery" style={{marginBottom: '1rem'}}>
                <div className="grid-y small-12 category">
                    <h6>Контактные данные</h6>
                    <div className="grid-x small-12">
                        <div className="grid-y small-6" style={{paddingRight: '1rem'}}>
                            <label className="grid-x">Имя<span className={'required'}>*</span></label>
                            <div className='grid-x'>
                                <input className="small-12" type='text' value={this.state.name}
                                       onChange={(e) => this.setState({name: e.target.value})}
                                       onBlur={() => this.updateName()}/>
                            </div>
                        </div>
                        {phones.length === 0 ?
                            <div className="grid-y small-6">
                                <CustomerPhone additional={phones.length > 0} onAddPhone={this.onAddPhone}/>
                            </div>
                            :
                            <div className="grid-y small-6">
                                <label className="grid-x">Телефон<span className={'required'}>*</span></label>
                                <div className='grid-x'>
                                    <select className='grid-x small-12'
                                            onChange={(e) => this.onChangeMainPhone(parseInt(e.target.value))} value={this.state.mainPhone.id}>
                                        {phones.map(item =>
                                            <option key={item.id} value={item.id}>
                                                {item.id !== -1 ? this.maskPhone(item.phone) : item.phone}
                                            </option>
                                        )}
                                    </select>
                                </div>
                                {this.state.addPhone === true ?
                                    <CustomerPhone additional={phones.length > 0} onAddPhone={this.onAddPhone}/> : null
                                }
                            </div>
                        }
                    </div>
                </div>
                <div className="grid-y small-12 category">
                    <h6>Место получения заказа</h6>
                    <div className="grid-x small-12">
                        <div className="grid-y small-6">
                            <label className="grid-x">Укажите дом на карте</label>
                            <div className='map-container'>
                                <OSMMap address={this.state.address} onChangeAddress={this.onChangeAddress}/>
                            </div>
                        </div>
                        <div className="grid-y small-6">
                            <div className='grid-x align-justify'>
                                <label className="grid-x">Выберите из списка</label>
                                {(this.state.addresses !== undefined && this.state.addresses.length > 0) && this.state.custom === true ?
                                <span className='grid-x gray-text' style={{textDecoration: 'underline'}}
                                      onClick={() => this.clearAddress()}>сброс</span>:null}
                            </div>
                            <div className='grid-x'>
                                {this.state.addresses === undefined || this.state.custom === true ?
                                    <OSMSearch address={this.state.address} onChangeAddress={this.onChangeAddress}
                                               onAddAddress={this.onAddAddress} addressId={this.state.addressId}/>
                                    :
                                    <div className="grid-x small-12">
                                        {this.state.addresses.map((item) =>
                                            <div key={item.id} id={'address-' + item.id}
                                                 onClick={(e) => this.makeActiveAddress(item, e)}
                                                 className={Object.entries(this.state.address).length > 0 && this.state.address.id === item.id ?
                                                     'grid-x small-12 align-justify address-item active' : 'grid-x small-12 align-justify address-item'}>
                                                <div className='grid-x align-middle'>
                                                    <input type={'radio'} name={'active-address'}
                                                           checked={Object.entries(this.state.address).length > 0 && this.state.address.id === item.id}
                                                           onChange={(e) => this.makeActiveAddress(item, e)}/>
                                                    <span className='gray-text'
                                                    style={{fontWeight: 'bold'}}>{OrderDelivery.buildAddress(item)}</span>
                                                </div>
                                                <i className="fas fa-pencil-alt"/>
                                            </div>
                                        )}
                                        <div className='grid-x small-12'>
                                            <span className='gray-text' style={{color: '#0db14b'}}
                                            onClick={() => this.addAddress()}>+ добавить адрес</span>
                                        </div>
                                    </div>
                                }
                            </div>
                        </div>
                    </div>
                </div>
                <div className="grid-y small-12 category">
                    <h6>Дата и время доставки</h6>
                    <div className="grid-x small-12">
                        <DeliveryDate availableDates={this.state.availableDates} selectedDate={this.state.selectedDate}
                                      onSelectedDateChange={this.onSelectedDateChange}/>
                        <DeliveryTime availableTimes={this.state.availableTimes} selectedTimes={this.state.selectedTimes}
                                      onChangeSelectedTimes={this.onChangeSelectedTimes}/>
                    </div>
                </div>
                <div className="grid-x small-12 align-right">
                    <button className="button warning button-next" type={'button'} onClick={() => this.toNextStep()}>
                        Далее
                    </button>
                </div>
                <button className="back-button" onClick={() => OrderDelivery.back()}>
                    <i className="fas fa-chevron-left"/>
                    К корзине
                </button>
            </div>
        )
    }
}

export default OrderDelivery;