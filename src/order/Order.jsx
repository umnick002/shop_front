import React from "react";
import OrderItems from "./OrderItems";
import OrderDelivery from "./OrderDelivery";

class Order extends React.Component {
    constructor(props) {
        super(props);
    }

    static onChangeActive(value) {
        if (window.location.pathname.indexOf('/order/delivery') !== -1 && value === 0) {
            window.location = '/order/items'
        } else if (window.location.pathname.indexOf('/order/items') !== -1 && value === 1) {
            window.location = '/order/delivery'
        }
    }

    render() {
        return (
            <div className="grid-y">
                {isMobile === false ?
                <div className="grid-x small-12 basket-header">
                    <div className={window.location.pathname.indexOf('/order/items') !== -1 ? "grid-x basket-step active" : "grid-x basket-step enabled"}
                         onClick={() => Order.onChangeActive(0)}>
                        <div className="step-number text-center">1</div>
                        <span>Корзина</span>
                    </div>
                    <i className="fas fa-chevron-right"/>
                    <div className={window.location.pathname.indexOf('/order/delivery') !== -1 ? "grid-x basket-step active" : "grid-x basket-step enabled"}
                         onClick={() => Order.onChangeActive(1)}>
                        <div className="step-number text-center">2</div>
                        <span>Адрес и время доставки</span>
                    </div>
                    <i className="fas fa-chevron-right"/>
                    <div className="grid-x basket-step disabled">
                        <div className="step-number text-center">3</div>
                        <span>Завершение</span>
                    </div>
                </div> : null}
                {window.location.pathname.indexOf('/order/items') !== -1 ?
                    <OrderItems customer={this.props.customer} onChangeOrder={this.props.onChangeOrder}/>
                    :
                    window.location.pathname.indexOf('/order/delivery') !== -1 ?
                    <OrderDelivery customer={this.props.customer}/>
                    : null
                }
            </div>
        )
    }
}

export default Order;