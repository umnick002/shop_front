import React from "react";
import ProductToCart from "../product/ProductToCart";
import Category from "../category/Category";

class OrderDetails extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        let items = {};
        const orderItems = this.props.orderItems;
        if (orderItems !== undefined) {
            orderItems.forEach(item => {
                if (item.sum > 0) {
                    if (items.hasOwnProperty(item.sellPrice.product.commonProduct.subCategory.name)) {
                        items[item.sellPrice.product.commonProduct.subCategory.name].push(item)
                    } else {
                        items[item.sellPrice.product.commonProduct.subCategory.name] = [item]
                    }
                }
            });
            orderItems.forEach(item => {
                if (item.sum === 0) {
                    if (items.hasOwnProperty('Нет в наличии')) {
                        items['Нет в наличии'].push(item);
                    } else {
                        items['Нет в наличии'] = [item];
                    }
                }
            })
        }
        let renderItems = [];
        for (let i in Object.entries(items)) {
            if (isMobile === false) {
                renderItems.push(
                    <div key={Object.entries(items)[i][0]} className="grid-y small-12 category">
                        {Object.entries(items)[i][0]}
                    </div>
                );
                Object.entries(items)[i][1].forEach((item, index) => {
                    let style = {};
                    if (parseInt(index + 1) < Object.entries(items)[i][1].length) {
                        style['borderBottom'] = "0.1rem solid #eee";
                    }
                    if (item.count === 0) {
                        style['opacity'] = "0.4";
                    }
                    renderItems.push(
                        <div key={item.id} className="grid-x small-12 item"
                             style={style}>
                            <div className="grid-x small-1 align-middle align-center">
                                <button className="remove-item">
                                    <i className="fas fa-times"/>
                                </button>
                            </div>
                            <div className="grid-x small-2 align-center">
                                <a className="align-center" rel="noopener noreferrer" target="_blank"
                                   href={'/products/' + item.sellPrice.product.commonProduct.id}>
                                    {item.sellPrice.product.commonProduct.contents.length > 0 ?
                                        <img key={item.id}
                                             src={staticHost + item.sellPrice.product.commonProduct.contents[0].file}
                                             style={{minHeight: "100px", maxHeight: "100px", cursor: "pointer"}}/>
                                        : <i className="far fa-2x fa-image"/>
                                    }
                                </a>
                            </div>
                            <div className="grid-x small-7 item-description">
                                <div className="grid-y small-12 title">
                                    <a rel="noopener noreferrer" target="_blank"
                                       href={'/products/' + item.sellPrice.product.commonProduct.id}>
                                        {item.sellPrice.product.commonProduct.name}
                                    </a>
                                </div>
                                <div className="grid-y small-12 sub-title">
                                    Артикул {item.sellPrice.product.number}
                                </div>
                                <div className="grid-y small-12 sub-title">
                                    Штука: {parseFloat(item.sellPrice.product.packVolume).toFixed(2)} {item.sellPrice.product.packVolumeUnit}
                                </div>
                            </div>
                            <div className="grid-x small-2 volume">
                                <div className="grid-x small-12 price-main">
                                    {item.count === 0 ? 0 : item.sum} р
                                </div>
                                <div className="grid-x small-12 price-second">
                                    {item.sellPrice.price} р/шт
                                </div>
                                <div className="grid-x small-12">
                                    <ProductToCart orderItem={item} activeProduct={item.sellPrice.product}
                                                   onChangeOrder={this.props.onChangeOrder}/>
                                </div>
                            </div>
                        </div>
                    )
                });
            } else {
                renderItems.push(
                    <div key={Object.entries(items)[i][0]} className="grid-y small-12 category">
                        {Object.entries(items)[i][0]}
                    </div>
                );
                Object.entries(items)[i][1].forEach((item, index) => {
                    let style = {};
                    if (parseInt(index + 1) < Object.entries(items)[i][1].length) {
                        style['borderBottom'] = "0.1rem solid #eee";
                    }
                    if (item.count === 0) {
                        style['opacity'] = "0.4";
                    }
                    renderItems.push(
                        <div key={item.id} className="grid-x small-12 item"
                             style={style}>
                            <div className="grid-y small-4 align-center">
                                <a className="grid-x align-center-middle" rel="noopener noreferrer" target="_blank"
                                   href={'/products/' + item.sellPrice.product.commonProduct.id}>
                                    {item.sellPrice.product.commonProduct.contents.length > 0 ?
                                        <img key={item.id}
                                             src={staticHost + item.sellPrice.product.commonProduct.contents[0].file}
                                             style={{minHeight: "100px", maxHeight: "100px", cursor: "pointer"}}/>
                                        : <i className="far fa-2x fa-image"/>
                                    }
                                </a>
                                <div className="grid-x align-center-middle">
                                    <button className="remove-item">
                                        <i className="fas fa-lg fa-trash"/>
                                    </button>
                                </div>
                            </div>
                            <div className="grid-y small-8 item-description">
                                <div className='grid-x'>
                                    <div className="grid-y small-12 title">
                                        <a rel="noopener noreferrer" target="_blank"
                                           href={'/products/' + item.sellPrice.product.commonProduct.id}>
                                            {item.sellPrice.product.commonProduct.name}
                                        </a>
                                    </div>
                                </div>
                                <div className='grid-x'>
                                    <div className="grid-x small-12 volume align-justify" style={{paddingLeft: 0}}>
                                        <div className="grid-x price-main">
                                            {item.count === 0 ? 0 : item.sum} р
                                        </div>
                                        <div className="grid-x">
                                            <ProductToCart orderItem={item} activeProduct={item.sellPrice.product}
                                                           onChangeOrder={this.props.onChangeOrder}/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    )
                });
            }
        }
        return (
            isMobile === false ?
            <div className="grid-x small-8 basket-details">
                {renderItems}
                <button className="back-button" onClick={() => Category.back()}>
                    <i className="fas fa-chevron-left"/>
                    Продолжить покупки
                </button>
            </div>
                :
            <div className='grid-x small-12'>
                <button className="back-button" onClick={() => Category.back()}>
                    <i className="fas fa-chevron-left"/>
                    Продолжить покупки
                </button>
                <div className="grid-x small-12 basket-details" style={{padding: '0 .5rem'}}>
                    {renderItems}
                </div>
            </div>
        )
    }
}

export default OrderDetails;