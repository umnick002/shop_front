import React from "react";
import OrderDetails from "./OrderDetails";
import OrderOverview from "./OrderOverview";

class OrderItems extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            order: {}
        }
    }

    componentDidMount() {
        fetch(host + '/order/').then(response => {
            return response.json();
        }).then(data => {
            console.log(data);
            this.setState({
                order: data
            })
        });
    }

    render() {
        return (
            <div className="grid-x small-12" style={{marginBottom: '1rem'}}>
                <OrderDetails orderItems={this.props.customer.orders[0].customerOrderItems}
                              onChangeOrder={this.props.onChangeOrder}/>
                <OrderOverview order={this.props.customer.orders[0]}/>
            </div>
        )
    }
}

export default OrderItems;