import React from "react";

class OrderOverview extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            promo: ''
        }
    }

    setPromo(promo) {
        this.setState({
            promo: promo
        })
    }

    static onChangeActive() {
        window.location = '/order/delivery'
    }

    render() {
        return (
            <div className={"grid-y basket-overview " + (isMobile === false ? 'desktop small-4' : 'mobile small-12')}>
                {this.props.order.sum < 1000 ?
                <div className="grid-x small-12 min-sum">
                    <div className="grid-x small-2 align-middle">
                        <i className="fas fa-2x fa-exclamation-triangle"/>
                    </div>
                    <div className="grid-x small-10">
                        Минимальная сумма заказа — 1000 руб.
                    </div>
                </div> : null}
                <div className="grid-y details">
                    <div className="grid-x small-12 promo">
                        <div className='title'>Промокод</div>
                        <div className='grid-x small-12 promo-container align-justify'>
                            <div className={'grid-x ' + (isMobile === false ? 'small-12' : '')}>
                                <input placeholder="Введите код" value={this.state.promo} onChange={(e) => this.setPromo(e.target.value)}/>
                            </div>
                            <div className={'grid-x ' + (isMobile === false ? 'small-12' : '')}>
                                <button className="button" type="button">Применить</button>
                            </div>
                        </div>
                    </div>
                    <div className="grid-x small-12 total">
                        <div className='grid-x small-12 align-justify title'>
                            <div>Итого:</div>
                            <div style={{color: '#0db14b'}}>{this.props.order.sum} р</div>
                        </div>
                        <div className='grid-x small-6 text-extra' style={{fontSize: '.8rem'}}>
                            Общая стоимость, без учета доставки
                        </div>
                        <br/>
                        <br/>
                        <div className='grid-x small-12 align-justify text-extra'>
                            <div>Всего товаров:</div>
                            <div>{this.props.order.pcs}</div>
                        </div>
                        <div className='grid-x small-12 align-justify text-extra'>
                            <div>Общий вес заказа:</div>
                            <div>{this.props.order.weight} кг</div>
                        </div>
                        <div className='grid-x small-12 align-justify text-extra'>
                            <div>Общий объём заказа:</div>
                            <div>{this.props.order.volume} л</div>
                        </div>
                        <br/>
                        <br/>
                        <div className="grid-x small-12">
                            <button className="small-12 grid-x button warning" type={'button'}
                                    onClick={() => OrderOverview.onChangeActive()}>Оформить заказ</button>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default OrderOverview;