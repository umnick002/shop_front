import React from "react";
import Product from "./product/Product"
import TopBar from "./menu/TopBar"
import Category from "./category/Category";
import Order from "./order/Order";
import Registration from "./auth/Registration";
import {BrowserRouter as Router, Route} from "react-router-dom";
import CategoryPage from "./category/CategoryPage";
import Admin from "./admin/Admin";

class Main extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            customer: {
                orders: [{customerOrderItems: []}]
            },
        };
        this.onChangeFavorite = this.onChangeFavorite.bind(this);
        this.onChangeOrder = this.onChangeOrder.bind(this);
        this.onChangeCategory = this.onChangeCategory.bind(this);
    }

    onChangeFavorite(favorites) {
        let customer = {...this.state.customer};
        customer.favorites = favorites;
        this.setState({customer})
    }

    onChangeOrder(productId, count) {
        fetch(host + '/order/item?product_id=' + productId + "&count=" + count, {
            method: "POST"
        }).then(response => {
            if (response.status === 200) {
                return response.json();
            } else if (response.status === 401) {
                Registration.registration({isGenerated: true}, this.onChangeOrder(productId, count));
            }
        }).then(data => {
            console.log(data);
            let customerOrderItems = [];
            let added = false;
            (this.state.customer.orders !== undefined ? this.state.customer.orders[0].customerOrderItems : []).forEach(item => {
                if (item.sellPrice.product.id !== data.sellPrice.product.id) {
                    customerOrderItems.push(item);
                } else {
                    customerOrderItems.push(data);
                    added = true;
                }
            });
            if (!added) {
                customerOrderItems.push(data);
            }
            let customer = {...this.state.customer};
            customer.orders = [data.customerOrder];
            customer.orders[0].customerOrderItems = customerOrderItems;
            this.setState({customer});
        })
    }

    componentDidMount() {
        fetch(host + '/details')
            .then(response => {
                if (response.status === 200) {
                    return response.json();
                } else if (response.status === 401) {

                }
            })
            .then(data => {
                if (data !== undefined) {
                    console.log(data);
                    this.setState({
                        customer: data
                    });
                }
            });
        fetch(host + `/category-list?full=${isMobile === true}`
        ).then(response => {
            return response.json();
        }).then(data => {
            console.log(data);
            this.setState({
                categories: data
            });
        });
    }

    onChangeCategory(category, withSubCategory=true) {
        if (withSubCategory === false || (withSubCategory === true && category.subCategories !== undefined)) {
            let newCategoryId = parseInt(category.id * 100) + (withSubCategory ? category.subCategories[0].id : 0);
            if (window.location.pathname.lastIndexOf('categories/') !== -1) {
                let categoryId = parseInt(window.location.pathname.slice(window.location.pathname.lastIndexOf('/') + 1));
                if (categoryId !== newCategoryId) {
                    window.history.pushState({'categoryId': newCategoryId}, "", `/categories/${newCategoryId}`);
                }
                let cat = category;
                if (withSubCategory === false) {
                    cat = {...category};
                    cat.subCategories = undefined;
                }
                this.setState({
                    category: cat
                })
            } else {
                location = `/categories/${newCategoryId}`;
            }
        }
    }

    render() {
        const path = window.location.pathname;
        return (
            <div className="grid-x align-center" style={path.indexOf('/admin') !== -1 ? {minWidth: "1000px"} : {}}>
                {path.indexOf('/admin') === -1 ?
                    <TopBar order={this.state.customer.orders !== undefined ? this.state.customer.orders[0] : {}} category={this.state.category}
                            onChangeOrder={this.onChangeOrder} onChangeCategory={this.onChangeCategory} categories={this.state.categories}/>
                : ''}
                <div style={path.indexOf('/admin') === -1 ? isMobile === false ? {minWidth: "1000px", maxWidth: "1000px", minHeight: '85vh'} :
                    {minWidth: "320px"} : {}}>
                    <Router>
                        <Route exact path={`/`} render={() =>
                            <Category customer={this.state.customer} main={true} categories={this.state.categories}
                                      category={this.state.category} onChangeCategory={this.onChangeCategory}
                                      onChangeFavorite={this.onChangeFavorite} onChangeOrder={this.onChangeOrder}/>} />
                        <Route path={`/categories/:id`} render={() =>
                            <Category customer={this.state.customer} main={false} categories={this.state.categories}
                                      category={this.state.category} onChangeCategory={this.onChangeCategory}
                                      onChangeFavorite={this.onChangeFavorite} onChangeOrder={this.onChangeOrder}/>} />
                        <Route path={`/products/:id`} render={() =>
                            <Product customer={this.state.customer} onChangeOrder={this.onChangeOrder}/>} />
                        <Route path={`/order`} render={() =>
                            <Order customer={this.state.customer} onChangeOrder={this.onChangeOrder}/>} />
                        <Route exact path={`/categories`} render={() =>
                            <CategoryPage categories={this.state.categories}/>} />

                        <Route path={`/admin`} component={Admin} />
                    </Router>
                </div>
                <div id={"fog"} className={'hide'}>
                </div>
            </div>
        )
    }
}

export default Main;