import React from "react";

class DeliveryDate extends React.Component {

    render() {
        let dates = this.props.availableDates || [];
        return (
            <div className="grid-x small-6">
                <div className='grid-x small-12 calendar-container'>
                    {dates.map((date, index) =>
                        <div key={index} className='grid-x small-4 weekday-container'>
                            <div className={'grid-x small-10 align-center weekday ' + (this.props.selectedDate === date ? 'active' : '')}
                            onClick={() => this.props.onSelectedDateChange(date)}>
                                {date.toLocaleDateString('ru-ru', { month: 'long', weekday: 'short', day: 'numeric' })}
                            </div>
                        </div>
                    )}
                </div>
            </div>
        )
    }
}

export default DeliveryDate;