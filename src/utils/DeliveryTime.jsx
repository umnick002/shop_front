import React from "react";

class DeliveryTime extends React.Component {

    render() {
        const availableTimes = this.props.availableTimes || [];
        const selectedTimes = this.props.selectedTimes || [];
        return (
            <div className="grid-x small-6">
                <div className="grid-x small-12 calendar-container">
                    {availableTimes.map((time, index) =>
                        <div key={index} className={"grid-x small-4 time-container "}
                             onClick={() => this.props.onChangeSelectedTimes(time)}>
                            <div className={'grid-x small-10 align-center time ' + (time.load !== 'LOW' ? 'high ' : ' ') + (selectedTimes.includes(time) ? 'active' : '')}>
                                {`${time.after.split(':')[0]}:${time.after.split(':')[1]} - ${time.before.split(':')[0]}:${time.before.split(':')[1]}`}
                            </div>
                            {time.load !== 'LOW' ?
                                <div className='time-warning'>
                                    <i className="fas fa-bolt"/>
                                </div>
                                : null
                            }

                        </div>
                    )}
                </div>
            </div>
        )
    }
}

export default DeliveryTime;