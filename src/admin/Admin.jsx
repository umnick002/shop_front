import React from "react";
import {BrowserRouter as Router, Route, NavLink} from "react-router-dom";
import AdminProducts from "./AdminProducts";
import AdminProduct from "./AdminProduct";
import AdminCustomerOrders from "./AdminCustomerOrders";
import AdminStockOrders from "./AdminStockOrders";
import AdminCustomerOrder from "./AdminCustomerOrder";
import AdminStockOrder from "./AdminStockOrder";
import LoginPage from "../auth/LoginPage";

class Admin extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <Router>
                <Route exact path={'/login'} component={LoginPage}/>
                <div className='grid-x small-12 align-center admin-page'>
                    <div className='grid-x small-3 align-center'>
                        <NavLink className='grid-x small-10 align-center-middle product-container'
                              activeClassName='is-active' to="/admin/products">Список продуктов</NavLink>
                    </div>
                    <div className='grid-x small-3 align-center'>
                        <NavLink className="grid-x small-10 align-center-middle product-container"
                              activeClassName='is-active' to="/admin/product">Добавить продукт</NavLink>
                    </div>
                    <div className='grid-x small-3 align-center'>
                        <NavLink className="grid-x small-10 align-center-middle product-container"
                              activeClassName='is-active' to="/admin/orders">Заказы пользователей</NavLink>
                    </div>
                    <div className='grid-x small-3 align-center'>
                        <NavLink className="grid-x small-10 align-center-middle product-container"
                              activeClassName='is-active' to="/admin/stock/orders">Заказы склада</NavLink>
                    </div>
                    <Route exact path={`/admin/products`} component={AdminProducts}/>
                    <Route exact path={`/admin/products/:id`} component={AdminProduct}/>
                    <Route exact path={`/admin/product`} component={AdminProduct}/>
                    <Route exact path={`/admin/orders`} component={AdminCustomerOrders}/>
                    <Route exact path={`/admin/orders/:id`} component={AdminCustomerOrder}/>
                    <Route exact path={`/admin/stock/orders`} component={AdminStockOrders}/>
                    <Route exact path={`/admin/stock/orders/:id`} component={AdminStockOrder}/>
                </div>
            </Router>
        )
    }
}

export default Admin;