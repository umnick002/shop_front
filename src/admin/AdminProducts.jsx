import React from "react";
import {Link} from "react-router-dom";

class AdminProducts extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            page: 1,
            pages: 1,
            commonProducts: [],
            categories: [],
            filters: {
                id: '',
                name: '',
                subCategory: {
                    name: 'Любая',
                    id: -1,
                    category: {name: 'Любая', id: -1, subCategories: []}
                }
            }
        };
    }

    componentDidMount() {
        this.fetchCommonProducts(this.state.filters);
        if (this.props.categories === undefined) {
            fetch(host + '/category-list?full=true'
            ).then(response => {
                return response.json();
            }).then(data => {
                console.log(data);
                data.forEach(item => {
                    item.subCategories.unshift({name: 'Любая', id: -1})
                });
                data.unshift({name: 'Любая', id: -1, subCategories: [{name: 'Любая', id: -1}]});
                this.setState({
                    categories: data
                });
            });
        } else {
            this.setState({
                categories: this.props.categories
            })
        }
    }

    fetchCommonProducts(filters, page=this.state.page) {
        fetch(host + '/products?page=' + (page || 1) +
            '&id=' + (filters.id || '') +
            '&name=' + (filters.name || '') +
            '&category_id=' + (filters.subCategory.category.id || -1) +
            '&sub_category_id=' + (filters.subCategory.id || -1)
        ).then(response => {
            return response.json();
        }).then(data => {
            console.log(data);
            this.setState({
                commonProducts: data.commonProducts,
                pages: data.pages
            });
        });
    }

    onChangeFilter(field, value) {
        let filters = {...this.state.filters};
        filters[field] = value;
        this.setState({filters})
    }

    onChangeCategory(e) {
        this.state.categories.forEach(item => {
            if (item.id === parseInt(e.target.value)) {
                let filters = {...this.state.filters};
                filters.subCategory = {name: 'Любая', id: -1, category: item};
                this.setState({filters});
                this.fetchCommonProducts(filters);
            }
        });
    }

    onChangeSubCategory(e) {
        let category = {};
        for (let i in this.state.categories) {
            if (this.state.categories[i].id === this.state.filters.subCategory.category.id) {
                category = this.state.categories[i];
                break
            }
        }
        for (let i in category.subCategories) {
            let item = category.subCategories[i];
            if (item.id === parseInt(e.target.value)) {
                item.category = {...category};
                delete item.category.subCategories;
                let filters = {...this.state.filters};
                filters.subCategory = item;
                this.setState({filters});
                this.fetchCommonProducts(filters);
                return
            }
        }
    }

    addProduct(commonProduct, product) {
        if (product !== undefined) {
            product.commonProduct = commonProduct;
        }
        this.props.addProduct(product !== undefined ? product : commonProduct);
    }

    toPage(page) {
        this.setState({
            page: page
        });
        this.fetchCommonProducts(this.state.filters, page)
    }

    render() {
        const commonProducts = this.state.commonProducts || [];
        let categories = [];
        if (this.state.categories !== undefined) {
            this.state.categories.forEach(item =>
                categories.push({name: item.name, id: item.id})
            )
        }
        let subCategories = [];
        for (let i in this.state.categories) {
            if (this.state.categories[i].id === this.state.filters.subCategory.category.id) {
                this.state.categories[i].subCategories.forEach(item => subCategories.push({name: item.name, id: item.id}));
                break
            }
        }
        let pages = [];
        for (let i = 1; i <= this.state.pages; i++) {
            pages.push(
                <Link key={i} className={'page-selector ' + (this.state.page === i ? 'is-active' : '')}
                      to={`/admin/products?page=${i}`} onClick={() => this.toPage(i)}>{i}</Link>
            )
        }
        return (
            <div className="grid-x small-12 admin">
                <table className="unstriped hover">
                    <thead>
                        <tr>
                            <th>id</th>
                            <th>Фото</th>
                            <th>Название</th>
                            <th>Упаковки</th>
                            <th>Категория</th>
                            <th>Подкатегория</th>
                            <th>Дата изменения</th>
                        </tr>
                    </thead>
                    <thead>
                        <tr>
                            <th>
                                <input type="text" value={this.state.filters.id}
                                       onChange={(e) => this.onChangeFilter('id', e.target.value.replace(/\D/g,''))}
                                       onBlur={() => this.fetchCommonProducts(this.state.filters)}
                                />
                            </th>
                            <th></th>
                            <th>
                                <input type="text" value={this.state.filters.name}
                                       onChange={(e) => this.onChangeFilter('name', e.target.value)}
                                       onBlur={() => this.fetchCommonProducts(this.state.filters)}
                                />
                            </th>
                            <th></th>
                            <th>
                                <select id="category" onChange={(e) => this.onChangeCategory(e)}
                                        value={this.state.filters.subCategory.category.id}>
                                    {categories.map(item =>
                                        <option key={item.id} value={item.id}>
                                            {item.name}
                                        </option>
                                    )}
                                </select>
                            </th>
                            <th>
                                <select id="subCategory" value={this.state.filters.subCategory.id}
                                        onChange={(e) => this.onChangeSubCategory(e)}>
                                    {subCategories.map(item =>
                                        <option key={item.id} value={item.id}>
                                            {item.name}
                                        </option>
                                    )}
                                </select>
                            </th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                    {commonProducts.map(item =>
                        <tr key={item.id}>
                            <th style={{width: "7rem"}}>
                                <Link to={'/admin/products/' + item.id}>{item.id}</Link>
                            </th>
                            <th className={'table-img'}>
                                <Link to={'/admin/products/' + item.id}>
                                    <img src={staticHost + item.contents[0].file}/>
                                </Link>
                            </th>
                            <th style={{width: "20rem", fontSize: '0.9rem'}}>
                                <Link to={'/admin/products/' + item.id}>{item.name}</Link>
                            </th>
                            <th style={{width: "12rem"}}>
                                {item.products && !item.byWeight ? item.products.map(product =>
                                    <div key={product.id} className="grid-x align-center-middle" style={{margin: "0.2rem 0"}}>
                                        <div className={this.props.categories ? "grid-x small-6 align-center-middle" : "grid-x"}>
                                            {parseFloat(product.packVolume).toFixed(2)} {product.packVolumeUnit}
                                        </div>
                                        {this.props.categories ?
                                            <div className="grid-x small-6">
                                                <button className="button" type="button"
                                                onClick={() => this.addProduct(item, product)}>
                                                    Выбрать
                                                </button>
                                            </div>
                                            : null
                                        }
                                    </div>
                                ) : item.products ?
                                    <div>
                                        <div className="grid-x small-6">
                                            <button className="button" type="button"
                                                    onClick={() => this.addProduct(item)}>
                                                Выбрать
                                            </button>
                                        </div>
                                    </div> : null
                                }
                            </th>
                            <th style={{width: "12rem"}}>
                                {item.subCategory.category.name}
                            </th>
                            <th style={{width: "12rem"}}>
                                {item.subCategory.name}
                            </th>
                            <th style={{width: "12rem"}}>
                                {new Date(item.updated).toLocaleDateString('ru-ru',
                                    { year: 'numeric', month: 'long', day: 'numeric', hour: 'numeric', minute: 'numeric' })}
                            </th>
                        </tr>
                    )}
                    </tbody>
                </table>
                <div className="grid-x">
                    {pages}
                </div>
            </div>
        )
    }
}

export default AdminProducts;