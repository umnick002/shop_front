import React from "react";
import * as ReactDOM from "react-dom";

class Alert extends React.Component {
    static close() {
        ReactDOM.unmountComponentAtNode(document.querySelector('#popup-container'));
    }

    render() {
        return (
            <div id="popup">
                <div className={"callout alert-callout-subtle grid-y " + this.props.status}>
                    <strong>{this.props.text}</strong>
                    {this.props.errors.map((item, index) =>
                        <div key={index}>
                            {item}
                        </div>
                    )}
                    <button className="close-button" aria-label="Dismiss alert" type="button" onClick={() => Alert.close()}>
                        <span aria-hidden="true">⊗</span>
                    </button>
                </div>
            </div>
        )
    }
}

export default Alert;