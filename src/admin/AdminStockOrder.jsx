import React from "react";
import AdminProducts from "./AdminProducts";
import Alert from "./Alert";
import * as ReactDOM from "react-dom";

class AdminStockOrder extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            categories: [],
            order: {},
        };
        this.addProduct = this.addProduct.bind(this);
    }

    componentDidMount() {
        if (window.location.pathname.match(/(stock\/orders\/\d+)/g)) {
            fetch(host + window.location.pathname.replace('/admin', ''))
            .then(response => {
                return response.json();
            })
            .then(data => {
                console.log(data);
                this.setState({
                    order: data
                })
            });
        }
        fetch(host + '/category-list?full=true'
        ).then(response => {
            return response.json();
        }).then(data => {
            console.log(data);
            data.forEach(item => {
                item.subCategories.unshift({name: 'Любая', id: -1})
            });
            data.unshift({name: 'Любая', id: -1, subCategories: [{name: 'Любая', id: -1}]});
            this.setState({
                categories: data
            });
            this.setState({
                categories: data
            });
        });
    }

    onChangeVolume(orderItem, volume) {
        let orderItems = {...this.state.order.stockOrderItems};
        for (let i in orderItems) {
            if (orderItems[i].buyPrice.product.id === orderItem.buyPrice.product.id) {
                orderItems[i].volume = volume;
                this.setState({orderItems});
                return
            }
        }
    }

    static showProducts() {
        document.querySelector('.products').classList.remove('hide');
        document.querySelector('.show-products').classList.add('hide');
    }

    static hideProducts() {
        document.querySelector('.products').classList.add('hide');
        document.querySelector('.show-products').classList.remove('hide');
    }

    onChangePrice(orderItem, price) {
        let order = {...this.state.order};
        for (let i in order.stockOrderItems) {
            if (order.stockOrderItems[i].buyPrice.product.id === orderItem.buyPrice.product.id) {
                order.stockOrderItems[i].buyPrice.price = price;
                order.stockOrderItems[i].buyPrice.pricePerUnit = price / orderItem.buyPrice.product.packVolume;
                this.setState({order});
                return
            }
        }
    }

    addProduct(product) {
        let order = {...this.state.order};
        if (!order.stockOrderItems) {
            order.stockOrderItems = [];
        }
        for (let i in order.stockOrderItems) {
            if (order.stockOrderItems[i].buyPrice.product.id === product.id) {
                ReactDOM.render(<Alert text='Данный продукт уже в списке!' errors={[]} status={'warning'}/>,
                    document.querySelector('#popup-container'));
                return
            }
        }
        if (!product.activeBuyPrice) {
            product.activeBuyPrice = {
                price: '',
                pricePerUnit: '',
                product: product,
                productId: product.id,
            }
        }
        let p = {...product};
        if (p.commonProduct.byWeight !== true) {
            delete p.commonProduct.products;
        }
        delete p.activeBuyPrice;
        product.activeBuyPrice.product = p;
        order.stockOrderItems.push({
            volume: 1,
            stockOrderId: this.state.order.id,
            buyPrice: product.activeBuyPrice
        });
        this.setState({order});
        AdminStockOrder.hideProducts();
    }

    deleteOrderItem(productId) {
        let order = {...this.state.order};
        for (let i in order.stockOrderItems) {
            if (order.stockOrderItems[i].buyPrice.product.id === productId) {
                order.stockOrderItems.splice(i, 1);
                this.setState({order});
                return
            }
        }
    }

    saveOrder() {
        console.log(this.state.order);
        fetch(host + '/stock/orders/', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json;charset=UTF-8'
            },
            body: JSON.stringify(this.state.order)
        }).then(response => {
            return response.json();
        }).then(data => {
            console.log(data);
        });
    }

    changeStatus(status) {
        console.log(this.state.order.id, status)
        fetch(host + '/stock/orders/' + this.state.order.id + '?status=' + status, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json;charset=UTF-8'
            }
        }).then(response => {
            return response.json();
        }).then(data => {
            console.log(data);
            let order = {...this.state.order};
            order.status = data.status;
            this.setState({order});
        });
    }

    render() {
        let orderItems = [];
        if (this.state.order.stockOrderItems) {
            orderItems = this.state.order.stockOrderItems;
        }
        return (
            <div className="grid-x small-12 align-center admin">
                <div className="grid-x small-10 align-justify">
                    <div>Статус: {this.state.order.status}</div>
                    <div>Дата создания: {new Date(this.state.order.created).toLocaleDateString('ru-ru',
                        { weekday: 'short', year: 'numeric', month: 'long', day: 'numeric', hour: 'numeric', minute: 'numeric' })}</div>
                    <div>Дата обновления: {new Date(this.state.order.updated).toLocaleDateString('ru-ru',
                        { weekday: 'short', year: 'numeric', month: 'long', day: 'numeric', hour: 'numeric', minute: 'numeric' })}</div>
                </div>
                <form className={'grid-x small-12'} onSubmit={() => this.saveOrder()}>
                    <table className="unstriped hover">
                        <thead>
                            <tr>
                                <th>Количество</th>
                                <th>Фото</th>
                                <th>Наименование</th>
                                <th>Упаковка</th>
                                <th>Цена за упаровку</th>
                                <th>Приведённая цена</th>
                                {this.state.order && this.state.order.status === 'Оформляется' ?
                                    <th>Действия</th>:null
                                }
                            </tr>
                        </thead>
                        <tbody>
                            {orderItems.map((orderItem, index) =>
                                <tr key={index} className="order-item">
                                    <th>
                                        {this.state.order && this.state.order.status === 'Оформляется' ?
                                            <input className="volume text-center" value={orderItem.count}
                                                   onChange={(e) => this.onChangeVolume(orderItem, e.target.value)}/> :
                                            <span>{orderItem.count}</span>
                                        }
                                    </th>
                                    <th>
                                        <img src={staticHost + orderItem.buyPrice.product.commonProduct.contents[0].file}/>
                                    </th>
                                    <th>
                                        {orderItem.buyPrice.product.commonProduct.name}
                                    </th>
                                    <th>
                                        {parseFloat(orderItem.buyPrice.product.packVolume).toFixed(2)} {orderItem.buyPrice.product.packVolumeUnit}
                                    </th>
                                    <th style={{width: "8rem"}}>
                                        {this.state.order && this.state.order.status === 'Оформляется' ?
                                            <input style={{width: "4rem"}} className="text-center"
                                                   value={orderItem.buyPrice.price}
                                                   onChange={(e) => this.onChangePrice(orderItem, e.target.value)}/>
                                            :
                                            <span>{orderItem.buyPrice.price} </span>
                                        }
                                        р/шт
                                    </th>
                                    <th>
                                        {orderItem.buyPrice.pricePerUnit ?
                                        parseFloat(orderItem.buyPrice.pricePerUnit).toFixed(2) + ' р/' + orderItem.buyPrice.product.packVolumeUnit : ''}
                                    </th>
                                    {this.state.order && this.state.order.status === 'Оформляется' ?
                                        <th>
                                            <button className="button alert" type="button"
                                                    onClick={() => this.deleteOrderItem(orderItem.buyPrice.product.id)}>
                                                Удалить
                                            </button>
                                        </th>:null
                                    }
                                </tr>
                            )}
                        </tbody>
                    </table>
                    {this.state.order && this.state.order.status === 'Оформляется' ?
                        <div className='grid-x small-12 align-right'>
                            <button className="button button-medium show-products" type="button"
                                    onClick={() => AdminStockOrder.showProducts()}>
                                Добавить
                            </button>
                            <div className="hide small-12 products">
                                <h5><b>Выберите продукт</b></h5>
                                <AdminProducts categories={this.state.categories} addProduct={this.addProduct}/>
                                <button className="button button-medium warning" type="button"
                                        onClick={() => AdminStockOrder.hideProducts()}>
                                    Отменить
                                </button>
                            </div>
                        </div>:null
                    }
                    <div className="grid-x small-12 align-right">
                        <span style={{margin: '.5rem 0 1rem 0'}}>Сумма: <b>{this.state.order.sum} р</b></span>
                    </div>
                    <div className="grid-x small-12 align-right buttons">
                        {this.state.order && !['Выполнено', 'Отменено', 'Оформляется'].includes(this.state.order.status) ?
                            <button className="button button-large show-products alert"
                                    onClick={(e) => {e.preventDefault();this.changeStatus('CANCEL')}}>
                                Отменить
                            </button>:null
                        }
                        {this.state.order && this.state.order.status === 'Оформляется' ?
                            <button className="button button-large show-products success"
                                    type="submit">
                                Сохранить
                            </button>:null
                        }
                        {this.state.order && this.state.order.status === 'Оформляется' ?
                            <button className="button button-large show-products warning"
                                    onClick={(e) => {e.preventDefault();this.changeStatus('FINALIZE')}}>
                                Оформить
                            </button>:null
                        }
                        {this.state.order && this.state.order.status === "Отменено" ?
                            <button className="button button-large show-products warning"
                                    onClick={(e) => {e.preventDefault();this.changeStatus('DRAFT')}}>
                                Вернуть в работу
                            </button>:null
                        }
                        {this.state.order && this.state.order.status === "Оформлено" ?
                            <button className="button button-large show-products warning"
                                    onClick={(e) => {e.preventDefault();this.changeStatus('ORDER')}}>
                                Заказать
                            </button>:null
                        }
                        {this.state.order && this.state.order.status === "Заказано" ?
                            <button className="button button-large show-products warning"
                                    onClick={(e) => {e.preventDefault();this.changeStatus('DELIVER')}}>
                                Доставлено
                            </button>:null
                        }
                        {this.state.order && this.state.order.status === "Доставлено" ?
                            <button className="button button-large show-products warning"
                                    onClick={(e) => {e.preventDefault();this.changeStatus('COMPLETE')}}>
                                Выполнено
                            </button>:null
                        }
                    </div>
                </form>
            </div>
        )
    }
}

export default AdminStockOrder;