import React from "react";
import {Link} from "react-router-dom";

class AdminCustomerOrder extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            order: {},
        };
    }

    componentDidMount() {
        if (window.location.pathname.match(/(orders\/\d+)/g)) {
            fetch(host + window.location.pathname.replace('/admin', ''))
                .then(response => {
                    return response.json();
                })
                .then(data => {
                    console.log(data);
                    this.setState({
                        order: data
                    })
                });
        }
    }

    onChangeVolume(orderItem, volume) {
        let orderItems = {...this.state.order.customerOrderItems};
        for (let i in orderItems) {
            if (orderItems[i].sellPrice.product.id === orderItem.sellPrice.product.id) {
                orderItems[i].volume = volume;
                this.setState({orderItems});
                return
            }
        }
    }

    onChangePrice(orderItem, price) {
        let order = {...this.state.order};
        for (let i in order.customerOrderItems) {
            if (order.customerOrderItems[i].sellPrice.product.id === orderItem.sellPrice.product.id) {
                order.customerOrderItems[i].sellPrice.price = price;
                order.customerOrderItems[i].sellPrice.pricePerUnit = price / orderItem.sellPrice.product.packVolume;
                this.setState({order});
                return
            }
        }
    }

    deleteOrderItem(productId) {
        let order = {...this.state.order};
        for (let i in order.customerOrderItems) {
            if (order.customerOrderItems[i].sellPrice.product.id === productId) {
                order.customerOrderItems.splice(i, 1);
                this.setState({order});
                return
            }
        }
    }

    saveOrder() {
        console.log(this.state.order);
        fetch(host + '/orders/', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json;charset=UTF-8'
            },
            body: JSON.stringify(this.state.order)
        }).then(response => {
            return response.json();
        }).then(data => {
            console.log(data);
        });
    }

    changeStatus(status) {
        console.log(this.state.order.id, status)
        fetch(host + '/stock/orders/' + this.state.order.id + '?status=' + status, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json;charset=UTF-8'
            }
        }).then(response => {
            return response.json();
        }).then(data => {
            console.log(data);
            let order = {...this.state.order};
            order.status = data.status;
            this.setState({order});
        });
    }

    render() {
        let orderItems = [];
        if (this.state.order.customerOrderItems) {
            orderItems = this.state.order.customerOrderItems;
        }
        return (
            <div className="admin">
                <div className="grid-x align-justify">
                    <div>Статус: {this.state.order.status}</div>
                    <div>Дата создания: {new Date(this.state.order.created).toLocaleDateString('ru-ru',
                        { weekday: 'short', year: 'numeric', month: 'long', day: 'numeric', hour: 'numeric', minute: 'numeric' })}</div>
                    <div>Дата обновления: {new Date(this.state.order.updated).toLocaleDateString('ru-ru',
                        { weekday: 'short', year: 'numeric', month: 'long', day: 'numeric', hour: 'numeric', minute: 'numeric' })}</div>
                </div>
                <form onSubmit={() => this.saveOrder()}>
                    <table className="unstriped hover">
                        <thead>
                        <tr>
                            <th>Количество</th>
                            <th>Фото</th>
                            <th>Наименование</th>
                            <th>Упаковка</th>
                            <th>Цена за упаровку</th>
                            {/*<th>Приведённая цена</th>*/}
                            {this.state.order && this.state.order.status === 'Оформляется' ?
                                <th>Действия</th>:null
                            }
                        </tr>
                        </thead>
                        <tbody>
                        {orderItems.map((orderItem, index) =>
                            <tr key={index} className="order-item">
                                <th>
                                    {this.state.order && this.state.order.status === 'Оформляется' ?
                                        <input className="volume text-center" value={orderItem.count}
                                               onChange={(e) => this.onChangeVolume(orderItem, e.target.value)}/> :
                                        <span>{orderItem.count}</span>
                                    }
                                </th>
                                <th className={'table-img'}>
                                    <Link to={`/admin/products/${orderItem.sellPrice.product.id}`}>
                                        <img src={staticHost + orderItem.sellPrice.product.commonProduct.contents[0].file}/>
                                    </Link>
                                </th>
                                <th>
                                    {orderItem.sellPrice.product.commonProduct.name}
                                </th>
                                <th>
                                    {parseFloat(orderItem.sellPrice.product.packVolume).toFixed(2)} {orderItem.sellPrice.product.packVolumeUnit}
                                </th>
                                <th>
                                    {orderItem.sellPrice.price} р
                                </th>
                                {/*<th>*/}
                                    {/*{orderItem.sellPrice.pricePerUnit ?*/}
                                        {/*orderItem.sellPrice.pricePerUnit + ' р/' + orderItem.sellPrice.product.packVolumeUnit : ''}*/}
                                {/*</th>*/}
                                {this.state.order && this.state.order.status === 'Оформляется' ?
                                    <th>
                                        <button className="button alert" type="button"
                                                onClick={() => this.deleteOrderItem(orderItem.sellPrice.product.id)}>
                                            Удалить
                                        </button>
                                    </th>:null
                                }
                            </tr>
                        )}
                        </tbody>
                    </table>
                    <div className="grid-x align-right" style={{marginBottom: "1rem"}}>
                        Сумма: <b>{this.state.order.sum} р</b>
                    </div>
                    <div className="grid-x align-right buttons">
                        {this.state.order && !['Выполнено', 'Отменено', 'Оформляется', 'Доставлено', 'Отправлено', undefined]
                            .includes(this.state.order.status) ?
                            <button className="button button-large show-products alert"
                                    onClick={(e) => {e.preventDefault();this.changeStatus('CANCEL')}}>
                                Отменить
                            </button>:null
                        }
                        {this.state.order && !['Оформляется', undefined].includes(this.state.order.status) ?
                            <button className="button button-large show-products success"
                                    type="submit">
                                Сохранить
                            </button>:null
                        }
                        {this.state.order && this.state.order.status === "Заказано" ?
                            <button className="button button-large show-products warning"
                                    onClick={(e) => {e.preventDefault();this.changeStatus('DRAFT')}}>
                                Вернуть в работу
                            </button>:null
                        }
                    </div>
                </form>
            </div>
        )
    }
}

export default AdminCustomerOrder;