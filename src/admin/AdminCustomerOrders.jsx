import React from "react";

class AdminCustomerOrders extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            categories: [],
            order: {},
            stocks: [],
            activeStock: {}
        };
    }

    componentDidMount() {
        this.fetchOrders(0);
        fetch(host + '/stocks'
        ).then(response => {
            return response.json();
        }).then(data => {
            console.log(data);
            this.setState({
                stocks: data,
                activeStock: data[0]
            });
        });
    }

    fetchOrders(page, stock, statuses) {
        let statusesParams = '';
        if (statuses !== undefined) {
            for (let i in statuses) {
                statusesParams += '&statuses=' + statuses[i];
            }
        }
        if (statusesParams === '') {
            statusesParams = '&statuses=';
        }
        fetch(host + '/orders?stock=' + stock + '&page=' + page + statusesParams
        ).then(response => {
            return response.json();
        }).then(data => {
            console.log(data);
            this.setState({
                orders: data
            });
        });
    }

    static toOrder(id) {
        window.location = '/admin/orders/' + id;
    }

    createOrder() {
        console.log(JSON.stringify({stockId: parseInt(this.state.activeStock.id)}))
        fetch(host + '/orders/', {
            method: "POST",
            body: JSON.stringify({stockId: parseInt(this.state.activeStock.id)}),
            headers: {
                "Content-Type": "application/json;charset=UTF-8"
            }
        }).then(response => {
            return response.json();
        }).then(data => {
            console.log(data);
            window.location = '/admin/orders/' + data.id
        });
    }

    render() {
        const orders = this.state.orders || [];
        return (
            <div className="grid-x small-12 admin">
                <table className="unstriped hover">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Статус</th>
                        <th>Пользователь</th>
                        <th>Дата создания</th>
                        <th>Дата изменения</th>
                        <th>Количество позиций</th>
                        <th>Сумма</th>
                    </tr>
                    </thead>
                    <tbody>
                    {orders.map(item =>
                        <tr key={item.id} onClick={() => AdminCustomerOrders.toOrder(item.id)}
                            style={{cursor: "pointer"}}>
                            <th>{item.id}</th>
                            <th>{item.status}</th>
                            <th>{item.customer.username}</th>
                            <th>{new Date(item.created).toLocaleDateString('ru-ru',
                                { weekday: 'short', year: 'numeric', month: 'long', day: 'numeric', hour: 'numeric', minute: 'numeric' })}
                            </th>
                            <th>{new Date(item.updated).toLocaleDateString('ru-ru',
                                { weekday: 'short', year: 'numeric', month: 'long', day: 'numeric', hour: 'numeric', minute: 'numeric' })}
                            </th>
                            <th>{item.pcs}</th>
                            <th>{item.sum}</th>
                        </tr>
                    )}
                    </tbody>
                </table>
            </div>
        )
    }
}

export default AdminCustomerOrders;