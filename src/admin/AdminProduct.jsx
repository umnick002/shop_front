import React from "react";
import Alert from "./Alert";
import * as ReactDOM from "react-dom";

class AdminProduct extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            commonProduct: {
                subCategory: {
                    name: ' -- выберите подкатегорию -- ',
                    id: -1,
                    category: {name: ' -- выберите категорию -- ', id: -1, subCategories: []}
                },
                contents: [],
                products: [],
                properties: [],
                name: '',
                description: '',
                byWeight: false
            },
            properties: [],
            categories: [],
            volume: 0,
            weight: 0,
            length: 0,
            width: 0,
            height: 0,
            number: 0
        };
        this.fileInput = React.createRef();
    }

    componentDidMount() {
        fetch(host + '/category-list?full=true'
        ).then(response => {
            return response.json();
        }).then(data => {
            console.log(data);
            this.setState({
                categories: data
            });
        });
        fetch(host + '/property-list'
        ).then(response => {
            return response.json();
        }).then(data => {
            console.log(data);
            this.setState({
                properties: data
            });
        });
        if (window.location.pathname.match(/(products\/\d+)/g)) {
            fetch(host + window.location.pathname.replace('/admin', '')
            ).then(response => {
                return response.json();
            }).then(data => {
                console.log(data);
                this.setState({
                    commonProduct: data
                });
            })
        }
    }

    onChangeCategory(e) {
        this.state.categories.forEach(item => {
            if (item.id === parseInt(e.target.value)) {
                let commonProduct = {...this.state.commonProduct};
                commonProduct.subCategory = {name: ' -- выберите подкатегорию -- ', id: -1, category: item};
                this.setState({commonProduct})
            }
        });
    }

    onChangeSubCategory(e) {
        let category = {};
        for (let i in this.state.categories) {
            if (this.state.categories[i].id === this.state.commonProduct.subCategory.category.id) {
                category = this.state.categories[i];
                break
            }
        }
        for (let i in category.subCategories) {
            let item = category.subCategories[i];
            if (item.id === parseInt(e.target.value)) {
                item.category = {...category};
                delete item.category.subCategories;
                let commonProduct = {...this.state.commonProduct};
                commonProduct.subCategory = item;
                this.setState({commonProduct});
                return
            }
        }
    }

    onChangeContents() {
        let extension = this.fileInput.current.files[0].name.slice(this.fileInput.current.files[0].name.lastIndexOf('.') + 1);
        fetch(host + '/contents?id=' + (this.state.commonProduct.id || ''), {
            method: "POST",
            body: this.fileInput.current.files[0],
            headers: {
                "Content-Type": ['jpg', 'jpeg'].includes(extension) ? 'image/jpeg' : ''
            }
        }).then(response => {
            return response.json();
        }).then(data => {
            let commonProduct = {...this.state.commonProduct};
            if (commonProduct.contents === undefined) {
                commonProduct.contents = [];
            }
            for (let i in commonProduct.contents) {
                if (commonProduct.contents[i].id === data.id) {
                    commonProduct.contents.splice(i, 1);
                    break;
                }
            }
            commonProduct.contents.push(data);
            this.setState({commonProduct});
        });
    }

    addVolume() {
        if (this.state.volume === undefined || this.state.weight === undefined || this.state.length === undefined ||
            this.state.width === undefined || this.state.height === undefined) {
            return;
        }
        let packVolumeUnit = document.querySelector('.volumes-add input[type=radio]:checked');
        if (packVolumeUnit !== null) {
            packVolumeUnit = packVolumeUnit.value;
        } else {
            return
        }
        let commonProduct = {...this.state.commonProduct};
        let volumes = commonProduct.products || [];
        for (let i in volumes) {
            let volume = volumes[i];
            if (volume.packVolume === this.state.volume && volume.packVolumeUnit === packVolumeUnit) {
                return
            }
        }
        volumes.push({
            packVolume: parseFloat(this.state.volume).toFixed(2),
            packVolumeUnit: packVolumeUnit,
            weight: parseFloat(this.state.weight).toFixed(2),
            length: parseInt(this.state.length),
            width: parseInt(this.state.width),
            height: parseInt(this.state.height),
            number: parseInt(this.state.number),
        });
        commonProduct.products = volumes;
        this.setState({commonProduct})
    }

    deleteVolume(e) {
        let commonProduct = {...this.state.commonProduct};
        for (let i in commonProduct.products) {
            let volume = commonProduct.products[i];
            if (e.target.parentElement.textContent === parseFloat(volume.packVolume).toFixed(2) + ' ' + volume.packVolumeUnit) {
                commonProduct.products.splice(i, 1);
                this.setState({commonProduct});
            }
        }
    }

    addProperty() {
        let propertyValue = document.querySelector('.property input[type=text]').value;
        if (propertyValue.length === 0) {
            return;
        }
        let propertyKey = document.querySelector('.property select').value;
        let commonProduct = {...this.state.commonProduct};
        if (commonProduct.properties === undefined) {
            commonProduct.properties = [];
        }
        for (let i in this.state.properties) {
            let propertyDict = this.state.properties[i];
            if (propertyDict.id === parseInt(propertyKey)) {
                let updated = false;
                for (let j in commonProduct.properties) {
                    if (commonProduct.properties[j].key.id === propertyDict.id) {
                        commonProduct.properties[j].value = propertyValue;
                        updated = true;
                        break
                    }
                }
                if (!updated) {
                    commonProduct.properties.push({value: propertyValue, key: propertyDict});
                }
                this.setState({commonProduct});
                break;
            }
        }
    }

    deleteProperty(e) {
        let commonProduct = {...this.state.commonProduct};
        for (let i in commonProduct.properties) {
            let property = commonProduct.properties[i];
            if (e.target.parentElement.parentElement.getAttribute('name') === property.key.name) {
                commonProduct.properties.splice(i, 1);
                this.setState({commonProduct});
            }
        }
    }

    changeName(e) {
        let commonProduct = {...this.state.commonProduct};
        commonProduct.name = e.target.value;
        this.setState({commonProduct});
    }

    changeDescription(e) {
        let commonProduct = {...this.state.commonProduct};
        commonProduct.description = e.target.value;
        this.setState({commonProduct});
    }

    deleteContent(e) {
        let commonProduct = {...this.state.commonProduct};
        for (let i in commonProduct.contents) {
            if (commonProduct.contents[i].id === parseInt(e.target.parentElement.getAttribute("name"))) {
                commonProduct.contents.splice(i, 1);
                this.setState({commonProduct});
                return;
            }
        }
    }

    saveProduct() {
        const commonProduct = this.state.commonProduct;
        if (!AdminProduct.validateProduct(commonProduct)) {
            return;
        }
        console.log(commonProduct)
        fetch(host + '/products', {
            method: "POST",
            body: JSON.stringify(commonProduct),
            headers: {
                "Content-Type": "application/json;charset=UTF-8"
            }
        }).then(response => {
            return response.json();
        }).then(data => {
            if (this.state.commonProduct.id === undefined) {
                location = '/admin/products/' + data.id;
            }
        });
    }

    static validateProduct(commonProduct) {
        let errors = [];
        if (commonProduct.name === undefined || commonProduct.name.length === 0) {
            errors.push("Заполните поле 'Название'");
        }
        if (commonProduct.description === undefined || commonProduct.description.length === 0) {
            errors.push("Заполните поле 'Описание'");
        }
        if (commonProduct.subCategory === undefined) {
            errors.push("Заполните поле 'Подкатегория'");
        }
        if (commonProduct.properties === undefined || commonProduct.properties.length === 0) {
            errors.push("Добавьте не менее одной 'Характеристики'");
        }
        if (commonProduct.products === undefined || commonProduct.products.length === 0) {
            errors.push("Добавьте не менее одного 'Продукты'");
        }
        if (commonProduct.contents === undefined || commonProduct.contents.length === 0) {
            errors.push("Добавьте не менее одного 'Фото'");
        }
        if (errors.length > 0) {
            ReactDOM.render(<Alert text='Заполните данные!' errors={errors} status={'warning'}/>,
                document.querySelector('#popup-container'));
        }
        return errors.length === 0;
    }

    changeProductField(field, e) {
        this.setState({
            [field]: e.target.value.replace(/[^\d.]/g, '')
        })
    }

    changeByWeight(checked) {
        let commonProduct = {...this.state.commonProduct};
        commonProduct.byWeight = checked;
        this.setState({commonProduct});
    }

    render() {
        let categories = [];
        if (this.state.commonProduct.subCategory.category.id === -1) {
            categories.push({name: this.state.commonProduct.subCategory.category.name, id: this.state.commonProduct.subCategory.category.id})
        }
        if (this.state.categories !== undefined) {
            this.state.categories.forEach(item =>
                categories.push({name: item.name, id: item.id})
            )
        }
        let subCategories = [];
        if (this.state.commonProduct.subCategory.id === -1) {
            subCategories.push({name: this.state.commonProduct.subCategory.name, id: this.state.commonProduct.subCategory.id})
        }
        for (let i in this.state.categories) {
            if (this.state.categories[i].id === this.state.commonProduct.subCategory.category.id) {
                this.state.categories[i].subCategories.forEach(item => subCategories.push({name: item.name, id: item.id}));
                break
            }
        }
        let properties = [];
        if (this.state.properties !== undefined) {
            this.state.properties.forEach(item =>
                properties.push({name: item.name, id: item.id})
            )
        }
        let renderProperties = [];
        if (this.state.commonProduct.properties !== undefined) {
            for (let i in this.state.commonProduct.properties) {
                let setProperty = this.state.commonProduct.properties[i];
                renderProperties.push(
                    <div className="grid-x small-12 set-property" name={setProperty.key.name} key={i}>
                        <div className="small-5"><b>{setProperty.key.name}:</b></div>
                        <div className="small-6"><span>{setProperty.value}</span></div>
                        <div className="grid-x small-1 align-right">
                            <i className="far fa-lg fa-times-circle"
                               onClick={(e) => this.deleteProperty(e)}
                            style={{display: "flex", alignItems: 'center'}}/>
                        </div>
                    </div>
                )
            }
        }
        let contents = [];
        for (let i in this.state.commonProduct.contents) {
            let content = this.state.commonProduct.contents[i];
            contents.push(
                <div className="content-container" key={content.id} name={content.id}>
                    <img src={staticHost + content.file} className="thumbnail" style={{width: '5rem'}}/>
                    <i className="far fa-lg fa-times-circle content-delete" onClick={(e) => this.deleteContent(e)}/>
                </div>
            )
        }
        let products = this.state.commonProduct.products || [];
        return (
            <div className="admin grid-x small-12">
                <form className="grid-x small-12 align-spaced" onSubmit={(e) => {e.preventDefault(); this.saveProduct()}}>
                    <div className="grid-x small-5 align-center">
                        <label className="grid-x small-12">
                            Название
                            <input id="name" type="text" value={this.state.commonProduct.name} onChange={(e) => this.changeName(e)}/>
                        </label>
                        <label className="grid-x small-12">
                            Описание
                            <textarea id="description" value={this.state.commonProduct.description} onChange={(e) => this.changeDescription(e)}/>
                        </label>
                        <label className="grid-x small-12">
                            Категория
                            <select id="category" onChange={(e) => this.onChangeCategory(e)}
                                    value={this.state.commonProduct.subCategory.category.id}>
                                {categories.map(item =>
                                    <option key={item.id} value={item.id} disabled={item.id === -1} hidden={item.id === -1}>
                                        {item.name}
                                    </option>
                                )}
                            </select>
                        </label>
                        <label className="grid-x small-12">
                            Подкатегория
                            <select id="subCategory" value={this.state.commonProduct.subCategory.id}
                                    onChange={(e) => this.onChangeSubCategory(e)}>
                                {subCategories.map(item =>
                                    <option key={item.id} value={item.id} disabled={item.id === -1} hidden={item.id === -1}>
                                        {item.name}
                                    </option>
                                )}
                            </select>
                        </label>
                        <div className="grid-x small-12">
                            <span className='small-12'>Фото (в порядке приоритета)</span>
                            <label className="button" style={{padding: "0.7rem"}}>
                                Загрузить
                                <input id="content" type="file" className="show-for-sr" ref={this.fileInput}
                                       onChange={() => this.onChangeContents()}/>
                            </label>
                            <div className="grid-x small-12 contents">
                                {contents}
                            </div>
                        </div>
                    </div>
                    <div className="grid-x small-5 align-center">
                        <label className="grid-x small-12 property">
                            Характеристики
                            <div className="grid-x small-12" style={{justifyContent: "space-between"}}>
                                <select id="property" className="grid-x small-3" style={{marginBottom: "0"}}>
                                    {properties.map(item =>
                                        <option key={item.id} value={item.id}>
                                            {item.name}
                                        </option>
                                    )}
                                </select>
                                <input className="small-5" type="text" style={{margin: "0 0.4rem"}}/>
                                <button className="button small-3" onClick={(e) => {e.preventDefault(); this.addProperty()}}>Добавить</button>
                            </div>
                            <div className="grid-x small-12 property-block" style={{position: "relative"}}>
                                {renderProperties}
                            </div>
                        </label>
                        <label className="grid-x small-12 align-middle">
                            Развесной
                            <input type='checkbox' name='by_weight' checked={this.state.commonProduct.byWeight}
                                   onChange={(e) => this.changeByWeight(e.target.checked)}
                                   style={{marginLeft: '1rem', transform: 'scale(1.4)'}}/>
                        </label>
                        <label className="grid-y small-12">
                            Продукты
                            <div className="grid-x small-12 volumes-add" style={{justifyContent: "space-between"}}>
                                <div className='grid-x small-12 align-middle'>
                                    <div className="grid-x small-2">Упаковка: </div>
                                    <input type="text" className="grid-x small-2"
                                           value={this.state.volume} onChange={(e) => this.changeProductField('volume', e)}
                                           style={{marginRight: '1rem', textAlign: "center"}}/>
                                    <label className="grid-x small-1" style={{alignItems: "center", fontSize: '1rem', fontWeight: "bold", marginRight: '1rem'}}>
                                        <input type="radio" name='volume-unit' value={'кг'}  className="grid-x small-2"
                                               style={{height: '1rem', width: "auto"}}/>
                                        <span>кг</span>
                                    </label>
                                    <label className="grid-x small-1" style={{alignItems: "center", fontSize: '1rem', fontWeight: "bold"}}>
                                        <input type="radio" name='volume-unit' value={'л'}  className="grid-x small-2"
                                               style={{height: '1rem', width: "auto"}}/>
                                        <span>л</span>
                                    </label>
                                </div>
                                <div className='grid-x small-12 align-middle'>
                                    <div className="grid-x small-2">Масса: </div>
                                    <input type="text" className="grid-x small-2"
                                           value={this.state.weight} onChange={(e) => this.changeProductField('weight', e)}
                                           style={{marginRight: '1rem', textAlign: "center"}}/> кг
                                </div>
                                <div className='grid-x small-12 align-middle'>
                                    <div className="grid-x small-2">Длина: </div>
                                    <input type="text" className="grid-x small-2"
                                           value={this.state.length} onChange={(e) => this.changeProductField('length', e)}
                                           style={{marginRight: '1rem', textAlign: "center"}}/> мм
                                </div>
                                <div className='grid-x small-12 align-middle'>
                                    <div className="grid-x small-2">Ширина: </div>
                                    <input type="text" className="grid-x small-2"
                                           value={this.state.width} onChange={(e) => this.changeProductField('width', e)}
                                           style={{marginRight: '1rem', textAlign: "center"}}/> мм
                                </div>
                                <div className='grid-x small-12 align-middle'>
                                    <div className="grid-x small-2">Высота: </div>
                                    <input type="text" className="grid-x small-2"
                                           value={this.state.height} onChange={(e) => this.changeProductField('height', e)}
                                           style={{marginRight: '1rem', textAlign: "center"}}/> мм
                                </div>
                                <div className='grid-x small-12 align-middle'>
                                    <div className="grid-x small-2">Артикул: </div>
                                    <input type="text" className="grid-x small-3"
                                           value={this.state.number} onChange={(e) => this.changeProductField('number', e)}
                                           style={{marginRight: '1rem', textAlign: "center"}}/>
                                </div>
                            </div>
                            <div className="grid-x align-right">
                                <button className="button button-medium small-3"
                                        onClick={(e) => {e.preventDefault(); this.addVolume()}}>
                                    Добавить
                                </button>
                            </div>
                            <div className="small-12 grid-x grid-margin-x align-left volumes"
                                 style={{fontSize: "1.2rem", userSelect: "none"}}>
                                {products.map((item, index) =>
                                    <div className="cell text-center small-3" key={index} id={index}>
                                        <div>
                                            {parseFloat(item.packVolume).toFixed(2)} {item.packVolumeUnit}
                                        </div>
                                        <i className="far fa-lg fa-times-circle" onClick={(e) => this.deleteVolume(e)}/>
                                    </div>
                                )}
                            </div>
                        </label>
                    </div>
                    <div className="grid-x small-12 align-right">
                        <div className="grid-x small-2 align-center">
                            <button className="button warning" type="submit" style={{padding: "0.7rem"}}>Сохранить</button>
                        </div>
                    </div>
                </form>
            </div>
        )
    }
}

export default AdminProduct;