import React from "react";
import ReactDOM from "react-dom";
import BottomBar from "./menu/BottomBar";
import Auth from "./auth/Auth";
import Main from "./Main";
import { Foundation } from 'foundation-sites';

$(document).foundation();

ReactDOM.render(<Main />, document.querySelector("#main"));
if (window.location.pathname.indexOf('/admin/') === -1) {
    ReactDOM.render(<BottomBar/>, document.querySelector("#bottom-bar"));
}
ReactDOM.render(<Auth />, document.querySelector("#auth"));
