import React from "react";
import ProductToCart from "../product/ProductToCart";

class Cart extends React.Component {
    constructor(props) {
        super(props);
    }

    static displayCart(e) {
        let icon = e.target.parentNode.querySelector('i');
        let cart = document.querySelector('.cart-details > div');
        if (document.body.clientWidth < 1580) {
            if (icon.classList.contains('fa-chevron-up')) {
                icon.classList.remove('fa-chevron-up');
                icon.classList.add('fa-chevron-down');
                cart.className = 'grid-x container';
            } else if (icon.classList.contains('fa-chevron-down')) {
                icon.classList.remove('fa-chevron-down');
                icon.classList.add('fa-chevron-up');
                cart.className = 'hide';
            }
        }
    }

    render() {
        const order = this.props.order;
        let cart = [];
        if (order !== undefined && order.customerOrderItems !== undefined) {
            order.customerOrderItems.map((orderItem, index) => {
                if (orderItem.count > 0) {
                    cart.push(
                        <div key={index} className="grid-x small-12 align-middle cart-item">
                            <div className="cell small-4">
                                <a href="#" className="text-center">
                                    {orderItem.sellPrice.product.commonProduct.contents.length > 0 ?
                                    <img src={staticHost + orderItem.sellPrice.product.commonProduct.contents[0].file}/>
                                    : <i className="far fa-2x fa-image"/>
                                    }
                                </a>
                            </div>
                            <div className="product-to-cart cell small-5 grid-x align-center">
                                <ProductToCart orderItem={orderItem} activeProduct={orderItem.sellPrice.product}
                                               onChangeOrder={this.props.onChangeOrder}/>
                            </div>
                            <div className="grid-x small-3 text-center">
                                {parseFloat(orderItem.sum).toFixed(2)}
                            </div>
                            <hr/>
                        </div>
                    )
                }
            })
        } else {
            cart =
                <div className="grid-y item-list align-center-middle" style={{height: "-webkit-fill-available", width: "100%"}}>
                    <div className="grid-y">
                        <i className="fas fa-8x fa-cart-plus"/>
                    </div>
                </div>;
        }

        return (
            <ul className="menu">
                <li>
                    <a className="cart-price text-center" href="/order/items" style={{width: '10rem'}}>
                        <i className="fas fa-lg fa-cart-arrow-down"/>
                        <span style={{width: '4rem'}}>{parseFloat(this.props.order.sum || 0).toFixed(2)} р</span>
                    </a>
                </li>
                <li className={"cart-details"}>
                    <a href="#" onClick={(e) => Cart.displayCart(e)}>
                        <i className={document.body.clientWidth > 1580 ? "fas fa-chevron-right" : "fas fa-chevron-up"}/>
                    </a>
                    <div className={document.body.clientWidth > 1580 ? "grid-x container full-cart" : "hide"}>
                        <div className="grid-y align-justify" style={{position: "relative", width: "100%"}}>
                            <div className="grid-y grid-x align-top cart-content">
                                {cart}
                            </div>
                            <div className="grid-y">
                                <a className="button warning" href={"/order/items"}>Оформить заказ</a>
                            </div>
                        </div>
                    </div>
                </li>
            </ul>
        )
    }
}

export default Cart;