import React from "react";

class BottomBar extends React.Component {
    render() {
        return (
            isMobile === false ?
            <div className="grid-x align-center" style={{height: '9vh'}}>
                <div className={"grid-x grid-padding-x small-up-6 content desktop"}>
                    <div className="cell grid-x item">
                        <a href="#">Доставка и оплата</a>
                    </div>
                    <div className="cell grid-x item">
                        <a href="#">Как заказать</a>
                    </div>
                    <div className="cell grid-x item">
                        <a href="#">Как вернуть</a>
                    </div>
                    <div className="cell grid-x item">
                        <a href="#">Вопросы и ответы</a>
                    </div>
                    <div className="cell grid-x item">
                        <i className="fas fa-phone"/>
                        <a href="#">+7 123 456-78-90</a>
                    </div>
                    <div className="cell grid-x item">
                        <i className="far fa-envelope"/>
                        <a href="#">Написать нам</a>
                    </div>
                </div>
            </div>
                :
            <div className="grid-x content mobile align-center">
                <div className="grid-x small-11" style={{borderBottom: ".05rem solid hsla(0,0%,85%,.2)"}}>
                    <div className="grid-x small-6 item">
                        <i className="fas fa-phone"/>
                        <a href="#">+7 123 456-78-90</a>
                    </div>
                    <div className="grid-x small-6 item">
                        <i className="far fa-envelope"/>
                        <a href="#">Написать нам</a>
                    </div>
                </div>
                <div className="grid-x small-11">
                    <div className="grid-x small-6 item">
                        <a href="#">Как заказать</a>
                    </div>
                    <div className="grid-x small-6 item">
                        <a href="#">Как вернуть</a>
                    </div>
                </div>
                <div className="grid-x small-11">
                    <div className="grid-x small-6 item">
                        <a href="#">Доставка и оплата</a>
                    </div>
                    <div className="grid-x small-6 item">
                        <a href="#">Вопросы и ответы</a>
                    </div>
                </div>
            </div>
        )
    }
}

export default BottomBar;