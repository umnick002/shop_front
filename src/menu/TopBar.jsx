import React from "react";
import Cart from "./Cart";

class TopBar extends React.Component {
    constructor(props) {
        super(props);
        window.addEventListener('resize', () => {
            TopBar.displayCart();
        });
        this.state = {
            displayMenu: false,
            clickedMenu: false,
            activeCategory: null,
        }
    }

    static displayCart() {
        let icon = document.querySelector('.order-details > a > i');
        if (icon === undefined) {
            return
        }
        if (document.body.clientWidth < 1580 && icon.classList.contains('fa-chevron-right')) {
            let cart = document.querySelector('.order-details > div');
            cart.className = 'hide';
            icon.className = 'fas fa-chevron-up';
        } else if (document.body.clientWidth >= 1580 &&
            (icon.classList.contains('fa-chevron-up') || icon.classList.contains('fa-chevron-down'))) {
            let cart = document.querySelector('.order-details > div');
            cart.className = 'grid-x container full-order';
            icon.className = 'fas fa-chevron-right';
        }
    }

    componentDidUpdate() {
        if (this.props.category === undefined && this.props.categories !== undefined) {
            let id = parseInt(window.location.pathname.slice(window.location.pathname.lastIndexOf('/') + 1));
            for (let category of this.props.categories) {
                if (category.id === parseInt(id/100)) {
                    if (id%100 === 0) {
                        this.onChangeSubCategory(category, null, false);
                        break;
                    } else {
                        if (category.subCategories !== undefined) {
                            for (let subCategory of category.subCategories) {
                                if (subCategory.id === id % 100) {
                                    this.onChangeSubCategory(category, subCategory, false);
                                    break
                                }
                            }
                            break
                        }
                    }
                }
            }
        }
    }

    displayMenu(displayMenu) {
        if (displayMenu === true) {
            document.querySelector("#fog").classList.remove('hide');
            document.querySelector("#fog").classList.add('grid-x');
            this.setState({
                displayMenu: displayMenu,
                clickedMenu: true,
                activeCategory: this.props.category || null
            })
        } else {
            document.querySelector("#fog").classList.remove('grid-x');
            document.querySelector("#fog").classList.add('hide');
            this.setState({
                displayMenu: displayMenu,
                clickedMenu: true,
                activeCategory: null
            })
        }
    }

    setActiveCategory(category) {
        const activeCategory = this.state.activeCategory;
        if (activeCategory !== null && activeCategory.id === category.id) {
            this.setState({activeCategory: null});
        } else {
            this.setState({activeCategory: category});
        }
    }

    onChangeSubCategory(category, subCategory, withAnimation=true) {
        if (withAnimation) {
            this.displayMenu(false);
        }
        let cat = {...category};
        cat.subCategories = subCategory !== null ? [subCategory] : undefined;
        this.props.onChangeCategory(cat, cat.subCategories !== undefined);
    }

    render() {
        return (
            isMobile === false ?
            <div className="grid-x small-12 align-center top-bar-container desktop" data-sticky-container style={{zIndex: 2}}>
                <div className="sticky grid-x" data-sticky data-options="marginTop: 0" data-stick-to="top">
                    <div className="grid-x small-12 align-center top-bar-container">
                        <div className="top-bar">
                            <div className="top-bar-left">
                                <ul className="menu">
                                    <li>
                                        <a className="logo" href="/">
                                            <i className="fas fa-lg fa-store"/>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <div className="top-bar-right">
                                <ul className="menu">
                                    <li>
                                        <form className="search grid-x">
                                            <input type="search" placeholder="Поиск"/>
                                            <button type="submit" className="button">
                                                <i className="fas fa-lg fa-search"/>
                                            </button>
                                        </form>
                                    </li>
                                    <li className={"profile"}>
                                        <a href="#">
                                            <i className="far fa-lg fa-user-circle"/>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i className={"far fa-lg fa-heart"}/>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <div className="top-bar-right">
                                <Cart order={this.props.order} onChangeOrder={this.props.onChangeOrder}/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
                :
            <div className="grid-x small-12 align-center top-bar-container mobile" data-sticky-container>
                <div className="sticky grid-x small-12" data-sticky  data-options="marginTop: 0;stickyOn: small;" data-stick-to="top">
                    <div className={"small-12 top-bar-container grid-x"}>
                        <div className="top-bar small-12 align-justify">
                            <div className="top-bar-left">
                                <ul className="menu">
                                    <li style={{borderRight: "0.05rem solid #ededed"}} onClick={(e) => {e.preventDefault();this.displayMenu(true)}}>
                                        <a className="menu-select" href="/">
                                            <i className="fas fa-lg fa-align-left"/>
                                        </a>
                                    </li>
                                    <li>
                                        <a className="logo" href="/">
                                            <i className="fas fa-lg fa-store"/>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <div className="top-bar-right">
                                <ul className="menu">
                                    <li>
                                        <form className="search grid-x">
                                            <input type="search" placeholder="Поиск" className="hide"/>
                                            <button type="submit" className="button">
                                                <i className="fas fa-lg fa-search"/>
                                            </button>
                                        </form>
                                    </li>
                                    <li className={"profile"}>
                                        <a href="#">
                                            <i className="far fa-lg fa-user-circle"/>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div className={"category-menu " +
                (this.state.displayMenu === true ? 'grid-x show-menu' : this.state.clickedMenu === true ? 'hide-menu' : 'hide')}>
                    <div className="grid-x small-12 category-menu-header">
                        <div className="top-bar small-12">
                            <div className="top-bar-left">
                                <ul className="menu">
                                    <li>
                                        <div className="grid-x align-middle">
                                            <i className="fas fa-lg fa-store"/>
                                            <span className="grid-x">Меню</span>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            <div className="top-bar-right">
                                <i className="fas fa-lg fa-times" onClick={() => {this.displayMenu(false)}}/>
                            </div>
                        </div>
                    </div>
                    <div className="grid-x small-12 category-menu-list">
                        <div className="grid-y small-12" style={{fontSize: "1rem", lineHeight: "2rem", color: "#3c3c3c"}}>
                            {(this.props.categories || []).map((category) =>
                                <div className="grid-x menu-category" key={category.id}
                                     onClick={() => this.setActiveCategory(category)}>
                                    <div className={"grid-x small-12 " +
                                    ((this.state.activeCategory !== null && category.id === this.state.activeCategory.id) ? 'active' : null)}>
                                        <div className="grid-x small-2 align-center-middle">
                                            <i className="fas fa-lg fa-cheese"/>
                                        </div>
                                        <div className="grid-x small-10 menu-category-item">
                                            <div className="grid-x small-10">
                                                <span>{category.name}</span>
                                            </div>
                                            <div className="grid-x small-2 align-center-middle">
                                                <i className={"fas " +
                                                ((this.state.activeCategory !== null && category.id === this.state.activeCategory.id) ? 'fa-chevron-up' : 'fa-chevron-down')}
                                                   style={{color: "#ccc"}}/>
                                            </div>
                                        </div>
                                    </div>
                                    <div className={"small-12 menu-subcategory-item " +
                                    ((this.state.activeCategory !== null && category.id === this.state.activeCategory.id) ? 'grid-y' : 'hide')}>
                                        {category.subCategories.map((subCategory) =>
                                            <div className={"grid-x menu-subcategory " +
                                            (this.props.category !== undefined &&
                                                this.props.category.id === category.id && this.props.category.subCategories !== undefined &&
                                                this.props.category.subCategories[0].id === subCategory.id ? 'selected' : '')}
                                                 key={subCategory.id}
                                                 onClick={() => this.onChangeSubCategory(category, subCategory)}>
                                                <div className="grid-x small-10">
                                                    <span>{subCategory.name}</span>
                                                </div>
                                                <div className="grid-x small-2 align-center-middle">
                                                    <i className="fas fa-chevron-right" style={{color: "#ccc"}}/>
                                                </div>
                                            </div>
                                        )}
                                    </div>
                                </div>
                            )}
                        </div>
                    </div>
                </div>
                <a className={"basket"} href={'/order/items'}>
                    <span>{parseFloat(this.props.order.sum).toFixed(2) + ' р'}</span>
                    <i className="fas fa-shopping-basket"/>
                </a>
            </div>
        )
    }
}

export default TopBar;