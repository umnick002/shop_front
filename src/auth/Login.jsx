import React from "react";
import Auth from "./Auth";

class Login extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            username: "8720788",
            password: "17384924092",
            isAuthenticated: false
        };
    }

    static login(username, password, callback) {
        fetch(host + '/login', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            credentials: 'include',
            body: 'username=' + username + '&password=' + password
        })
        .then(response => {
            if (response.status === 200) {
                document.querySelector('#auth').classList.remove('grid-x');
                document.querySelector('#auth').classList.add('hide');
            }
            if (callback !== undefined) {
                callback();
            } else {
                location.reload(true);
            }
        })
    }

    render() {
        return (
            <form className="grid-y small-12 grid-padding-y"
                  onSubmit={(e) => {e.preventDefault();Login.login(this.state.username, this.state.password)}}>
                <div className="cell grid-x">
                    <label className="small-12">
                        <input type="text" name="login"
                               onFocus={(e) => Auth.addActive(e)} onBlur={(e) => Auth.removeActive(e)}/>
                        <span>
                            Телефон, е-mail или логин
                        </span>
                    </label>
                </div>
                <div className="cell grid-x">
                    <label className="small-12">
                        <input type="text" name="password"
                               onFocus={(e) => Auth.addActive(e)} onBlur={(e) => Auth.removeActive(e)}/>
                        <span>
                            Пароль
                        </span>
                        <a href="#">
                            Напомнить
                        </a>
                    </label>
                </div>
                <div className="cell grid-x">
                    <button type="submit" className="button small-12">
                        Войти
                    </button>
                </div>
            </form>
        )
    }
}

export default Login;