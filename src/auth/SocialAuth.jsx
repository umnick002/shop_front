import React from "react";

class SocialAuth extends React.Component {
    render() {
        return (
            <div className="cell grid-x align-center social">
                <div className="grid-y small-8">
                    <div className="text-center soc-title">
                        Войти через соц. сети
                    </div>
                    <div className="grid-x align-spaced">
                        <a href="#" className="fb">
                            <i className="fab fa-3x fa-facebook"/>
                        </a>
                        <a href="#" className="vk">
                            <i className="fab fa-3x fa-vk"/>
                        </a>
                        <a href="#" className="ok">
                            <i className="fab fa-3x fa-odnoklassniki-square"/>
                        </a>
                    </div>
                </div>
            </div>
        )
    }
}

export default SocialAuth;