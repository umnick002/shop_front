import React from "react";
import Auth from "./Auth";
import Login from "./Login";

class Registration extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            name: '',
            email: '',
            phone: ''
        }
    }

    onChangeInput(field, value) {
        this.setState({
            [field]: value
        });
    }

    static registration(data, callback) {
        fetch(host + '/registration', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json;charset=UTF-8'
            },
            body: JSON.stringify(data)
        })
        .then(response => {
            if (response.status === 200) {
                return response.json()
            }
        })
        .then(data => {
            Login.login(data.username, data.password, callback)
        })
    }

    render() {
        return (
            <form className="grid-y small-12 grid-padding-y"
                  onSubmit={(e) => {e.preventDefault();Registration.registration(this.state, Auth.handleClose())}}>
                <div className="cell grid-x">
                    <label className="small-12">
                        <input type="text" name="login" required="required" value={this.state.name}
                               onChange={(e) => this.onChangeInput('firstName', e.target.value)}
                               onFocus={(e) => Auth.addActive(e)} onBlur={(e) => Auth.removeActive(e)}/>
                        <span>
                            Имя *
                        </span>
                    </label>
                </div>
                <div className="cell grid-x">
                    <label className="small-12">
                        <input type="text" name="login" value={this.state.email}
                               onChange={(e) => this.onChangeInput('email', e.target.value)}
                               onFocus={(e) => Auth.addActive(e)} onBlur={(e) => Auth.removeActive(e)}/>
                        <span>
                            E-mail
                        </span>
                    </label>
                </div>
                <div className="cell grid-x">
                    <label className="small-12">
                        <input type="text" name="login" value={this.state.phone}
                               onChange={(e) => this.onChangeInput('phone', e.target.value)}
                               onFocus={(e) => Auth.addActive(e)} onBlur={(e) => Auth.removeActive(e)}/>
                        <span>
                            Телефон
                        </span>
                    </label>
                </div>
                <div className="cell grid-x">
                    <button type="submit" className="button small-12">
                        Зарегистрироваться
                    </button>
                </div>
            </form>
        )
    }
}

export default Registration;