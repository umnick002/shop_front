import React from "react";
import Registration from "./Registration";
import Login from "./Login";

class Auth extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            active: 0
        }
    }

    static handleClose() {
        document.querySelector("#auth").classList.add('hide');
    }

    static addActive(e) {
        e.target.parentElement.querySelector('span').classList.add('active');
    }

    static removeActive(e) {
        if (e.target.value.length === 0) {
            e.target.parentElement.querySelector('span').classList.remove('active');
        }
    }

    handleActive(i) {
        this.setState({
            active: i
        })
    }

    render() {
        return (
            <div className="grid-x align-center-middle" style={{maxWidth: '1000px', minWidth: '1000px', height: '100%'}}
                 onClick={() => Auth.handleClose()}>
                <div className="content grid-x small-5 align-center" style={{position: 'relative', padding: '1rem'}}
                     onClick={(e) => e.stopPropagation()}>
                    <div className="grid-x small-8 small-up-2 text-center title">
                        <button id="login" type="button" className={this.state.active === 0 ? "cell active" : "cell"}
                                onClick={() => this.handleActive(0)}>Логин</button>
                        <button id="registration" type="button" className={this.state.active === 1 ? "cell active" : "cell"}
                                onClick={() => this.handleActive(1)}>Регистрация</button>
                    </div>
                    <div className={this.state.active === 0 ? 'grid-x small-10 align-center' : 'hide'}>
                        <Login/>
                    </div>
                    <div className={this.state.active === 1 ? 'grid-x small-10 align-center' : 'hide'}>
                        <Registration/>
                    </div>
                    <button className="close-button" type="button" onClick={() => Auth.handleClose()}>
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </div>
        )
    }
}

export default Auth;